package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.EmployeeDeleteDto;
import co.id.astra.pos.model.dto.EmployeeDto;
import co.id.astra.pos.model.dto.EmployeeEditDto;
import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.service.employee.EmployeeService;
import co.id.astra.pos.service.employee.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController extends BaseController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterEmployee employee){
        employeeService.save(employee);
        EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
        DefaultResponse<EmployeeDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(employeeDto);
        return response;
    }

    //karena restfull ga pertu /value lagi, karena yang membedakan adalah type mappingnya
    //jika ingin memakai type mapping yang sama maka di bedakan dengan parameternya
    //contoh @GetMapping(value=/{id})

//    @GetMapping
//    public DefaultResponse findAll(){
//        List<EmployeeDto> employees = employeeService.findAll().stream()
//                .map(employee -> modelMapper.map(employee, EmployeeDto.class))
//                .collect(Collectors.toList());
//        DefaultResponse<List<EmployeeDto>> response = new DefaultResponse(Boolean.TRUE);
//        response.setData(employees);
//        return response;
//    }

    @GetMapping
    public DefaultResponse findAll(){
        List<EmployeeDto> employees = employeeService.findAll().stream()
                .map(employee -> modelMapper.map(employee, EmployeeDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<EmployeeDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(employees);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody EmployeeEditDto employeeEditDto){
        //userEditDTO adalah kolom2 yang diperbolehkan untuk diedit (kontraktor data)
        // di map dulu ke Class User
        MasterEmployee employeeEdit = modelMapper.map(employeeEditDto, MasterEmployee.class); //yang tadinya dari userDTO di mapping ke user lagi

        MasterEmployee employee = employeeService.edit(employeeEdit);
        EmployeeEditDto resultUser = modelMapper.map(employee, EmployeeEditDto.class);
        DefaultResponse<EmployeeEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultUser);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody EmployeeDeleteDto employeeDeleteDto){
        // di map dulu ke Class Master Employee
        MasterEmployee employeeDelete = modelMapper.map(employeeDeleteDto, MasterEmployee.class);

        MasterEmployee employee = employeeService.delete(employeeDelete);
        EmployeeDeleteDto resultEmployee = modelMapper.map(employee, EmployeeDeleteDto.class);
        DefaultResponse<EmployeeDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultEmployee);
        return response;
    }
}
