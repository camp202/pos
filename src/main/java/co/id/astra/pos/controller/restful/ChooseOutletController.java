package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.ChooseOutletDetailDto;
import co.id.astra.pos.model.dto.ChooseOutletDto;
import co.id.astra.pos.model.dto.ChooseOutletEmployeeDto;
import co.id.astra.pos.service.chooseemployeeoutlet.ChooseOutletService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/outlets/")
public class ChooseOutletController {
    @Autowired
    private ChooseOutletService chooseOutletService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("{id}")
    public DefaultResponse getOutletByEmployeeId(@PathVariable("id") Long id) {
        List<ChooseOutletEmployeeDto> emps = chooseOutletService.getById(id).stream()
                .map(emp -> modelMapper.map(emp, ChooseOutletEmployeeDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<ChooseOutletEmployeeDto>> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(emps);
        return response;
    }
//    =================================TEST UNTUK AMBIL DETAIL OUTLET===============================

    @GetMapping("choose/{id}")
    public DefaultResponse goToOutletById(@PathVariable("id") Long id) {
        List<ChooseOutletDetailDto> chooseOutletDtos = chooseOutletService.goToOutletById(id).stream()
                .map(outlet -> modelMapper.map(outlet, ChooseOutletDetailDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<ChooseOutletDetailDto>> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(chooseOutletDtos);
        //response.setMessage("INI MESSAGE");
        return response;
    }
//    =========================================UNTUK TESTING========================================
//    @GetMapping
//    public DefaultResponse getOutlet() {
//        List<EmployeeChooseOutletDto> emps = chooseOutletService.findAll().stream()
//                .map(emp -> modelMapper.map(emp, EmployeeChooseOutletDto.class))
//                .collect(Collectors.toList());
//        DefaultResponse<List<EmployeeChooseOutletDto>> response = new DefaultResponse<>(Boolean.TRUE);
//        response.setData(emps);
//        return response;
//    }
}
