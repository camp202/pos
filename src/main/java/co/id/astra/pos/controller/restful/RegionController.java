package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.RegionDeleteDto;
import co.id.astra.pos.model.dto.RegionEditDto;
import co.id.astra.pos.model.dto.RegionGetDto;
import co.id.astra.pos.model.dto.RegionSaveDto;
import co.id.astra.pos.model.entity.master.MasterRegion;
import co.id.astra.pos.service.region.RegionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/region")
public class RegionController {
    @Autowired
    private RegionService regionService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save (@RequestBody MasterRegion region){
        regionService.save(region);
        RegionSaveDto regionDto = modelMapper.map(region, RegionSaveDto.class);
        DefaultResponse<RegionSaveDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(regionDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<RegionGetDto> regions = regionService.findAll().stream()
                .map(region -> modelMapper.map(region, RegionGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<RegionGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(regions);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable("id") Long id){
        List<RegionGetDto> regions = regionService.findAllById(id).stream()
                .map(region -> modelMapper.map(region, RegionGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<RegionGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(regions);
        return response;
    }
    @GetMapping("/getby/{provinceId}")
    public DefaultResponse findAll(@PathVariable("provinceId") Long provinceId){
        List<RegionGetDto> regions = regionService.findAllByMasterProvince(provinceId).stream()
                .map(region -> modelMapper.map(region, RegionGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<RegionGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(regions);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody RegionEditDto regionDto){
        MasterRegion regionEdit = modelMapper.map(regionDto, MasterRegion.class);

        MasterRegion region = regionService.edit(regionEdit);
        RegionEditDto resultRegion = modelMapper.map(region, RegionEditDto.class);
        DefaultResponse<RegionEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultRegion);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody RegionDeleteDto regionDto){
        MasterRegion regionDelete = modelMapper.map(regionDto, MasterRegion.class);

        MasterRegion region = regionService.delete(regionDelete);
        RegionDeleteDto resultRegion = modelMapper.map(region, RegionDeleteDto.class);
        DefaultResponse<RegionDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultRegion);
        return response;
    }
}
