package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.*;
import co.id.astra.pos.model.entity.master.MasterCategory;
import co.id.astra.pos.repository.ItemRepository;
import co.id.astra.pos.service.category.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    private ModelMapper modelMapper;


    @PostMapping
    public DefaultResponse save(@RequestBody MasterCategory masterCategory){
        categoryService.save(masterCategory);
        MasterCategoryDto masterCategoryDto = modelMapper.map(masterCategory, MasterCategoryDto.class);
        DefaultResponse<MasterCategoryDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(masterCategoryDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<CategoryGetItemDto> masterCategoryDtos = categoryService.findAll().stream()
                .map(masterCategory -> modelMapper.map(masterCategory, CategoryGetItemDto.class))
                .collect(Collectors.toList());

        for (CategoryGetItemDto categoryGetItemDto : masterCategoryDtos){
            MasterCategory masterCategory = new MasterCategory();
            masterCategory.setId(categoryGetItemDto.getId());
            List<ItemGet2Dto> itemGet2Dtos = itemRepository.findAllByMasterCategory(masterCategory).stream()
                    .map(masterItem -> modelMapper.map(masterItem, ItemGet2Dto.class)).collect(Collectors.toList());
            categoryGetItemDto.setItems(itemGet2Dtos);
        }

        DefaultResponse<List<CategoryGetItemDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(masterCategoryDtos);
        return response;
    }

    @GetMapping("{id}")
    public DefaultResponse getById(@PathVariable("id") Long id){
        List<CategoryEditDto> categoryEditDtos = categoryService.getById(id).stream()
                .map(masterCategory -> modelMapper.map(masterCategory, CategoryEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<CategoryEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(categoryEditDtos);
        return response;
    }


    @PutMapping
    public DefaultResponse edit(@RequestBody CategoryEditDto categoryEditDto){
        MasterCategory masterCategoryEdit = modelMapper.map(categoryEditDto,MasterCategory.class);

        MasterCategory masterCategory = categoryService.edit(masterCategoryEdit);
        CategoryEditDto resultCategory = modelMapper.map(masterCategory, CategoryEditDto.class);
        DefaultResponse<CategoryEditDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(resultCategory);
        return response;
    }

    @DeleteMapping()
    public DefaultResponse delete(@RequestBody CategoryDeleteDto categoryDeleteDto){
        MasterCategory masterCategoryDelete = modelMapper.map(categoryDeleteDto, MasterCategory.class);

        MasterCategory masterCategory = categoryService.delete(masterCategoryDelete);
        CategoryDeleteDto resultCategory = modelMapper.map(masterCategory, CategoryDeleteDto.class);
        DefaultResponse<CategoryDeleteDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(resultCategory);
        return response;
    }

    @DeleteMapping("{id}")
    public DefaultResponse deleteId(@PathVariable("id") Long id){
        MasterCategory category = categoryService.deleteId(id);
        CategoryDeleteDto resultCategory = modelMapper.map(category, CategoryDeleteDto.class);
        DefaultResponse<CategoryDeleteDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(resultCategory);
        return response;
    }



}
