package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.InstallmentDeleteDto;
import co.id.astra.pos.model.dto.InstallmentEditDto;
import co.id.astra.pos.model.dto.InstallmentGetDto;
import co.id.astra.pos.model.dto.MasterInstallmentDto;
import co.id.astra.pos.model.entity.master.MasterInstallment;
import co.id.astra.pos.service.installment.InstallmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/installment")
public class InstallmentController {

    @Autowired
    InstallmentService installmentService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterInstallment installment){
        installmentService.save(installment);
        MasterInstallmentDto installmentDto = modelMapper.map(installment, MasterInstallmentDto.class);
        DefaultResponse<MasterInstallmentDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(installmentDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<InstallmentGetDto> installmentDtos = installmentService.findAll().stream()
                .map(installment -> modelMapper.map(installment, InstallmentGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<InstallmentGetDto>> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(installmentDtos);
        return response;
    }

    @GetMapping("{id}")
    public DefaultResponse getById(@PathVariable("id") Long id){
        List<InstallmentEditDto> installmentEditDtos = installmentService.getById(id).stream()
                .map(masterInstallment -> modelMapper.map(masterInstallment, InstallmentEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<InstallmentEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(installmentEditDtos);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody InstallmentEditDto installmentEditDto){
        MasterInstallment installmentEdit = modelMapper.map(installmentEditDto, MasterInstallment.class);

        MasterInstallment installment = installmentService.edit(installmentEdit);
        InstallmentEditDto resultInstallment = modelMapper.map(installment, InstallmentEditDto.class);
        DefaultResponse<InstallmentEditDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(resultInstallment);
        return response;
    }

//    @DeleteMapping
//    public DefaultResponse delete(@RequestBody InstallmentDeleteDto installmentDeleteDto){
//        MasterInstallment installmentDelete = modelMapper.map(installmentDeleteDto, MasterInstallment.class);
//
//        MasterInstallment installment = installmentService.delete(installmentDelete);
//        InstallmentDeleteDto resultInstallment = modelMapper.map(installment, InstallmentDeleteDto.class);
//        DefaultResponse<InstallmentDeleteDto> response = new DefaultResponse<>(Boolean.TRUE);
//        response.setData(resultInstallment);
//        return response;
//    }
}
