package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.CountryDeleteDto;
import co.id.astra.pos.model.dto.CountryDto;
import co.id.astra.pos.model.dto.CountryEditDto;
import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.model.entity.master.MasterCountry;
import co.id.astra.pos.service.country.CountryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/api/country")
public class CountryController {
    @Autowired
    private CountryService countryService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterCountry masterCountry){
        countryService.save(masterCountry);
        CountryDto countryDto = modelMapper.map(masterCountry, CountryDto.class);
        DefaultResponse<CountryDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(countryDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<CountryDto> countries = countryService.findAll().stream()
                .map(masterCountry -> modelMapper.map(masterCountry, CountryDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<CountryDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(countries);
        return response;
    }

    @GetMapping("{id}")
    public DefaultResponse getById(@PathVariable("id") Long id){
        List<CountryEditDto> country = countryService.getById(id).stream()
                .map(masterCountry -> modelMapper.map(masterCountry,CountryEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<CountryEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(country);
        return response;
    }


//    @GetMapping ("/edit/{norm}")
//    public DefaultResponse edit (@PathVariable("norm") String norm){
//        Optional<Pasien> pasiens = pasienService.getPasien(norm);
//        DefaultResponse<Optional<Pasien>> response = new DefaultResponse(Boolean.TRUE);
//        response.setData(pasiens);
//        return response;
//    }

    @PutMapping
    public DefaultResponse edit(@RequestBody CountryEditDto countryEditDto){
        MasterCountry countryEdit = modelMapper.map(countryEditDto, MasterCountry.class);
        MasterCountry masterCountry = countryService.edit(countryEdit);
        CountryEditDto resultCountry = modelMapper.map(masterCountry, CountryEditDto.class);
        DefaultResponse<CountryEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultCountry);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete (@RequestBody CountryDeleteDto countryDeleteDto){
        MasterCountry countryDelete = modelMapper.map(countryDeleteDto, MasterCountry.class);
        MasterCountry masterCountry = countryService.delete(countryDelete);
        CountryDeleteDto resultCountry = modelMapper.map(masterCountry, CountryDeleteDto.class);
        DefaultResponse<CountryDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultCountry);
        return response;
    }

    @DeleteMapping ("/{id}")
    public DefaultResponse deleteById (@PathVariable("id") Long id){
        MasterCountry masterCountry = countryService.deleteById(id);
        CountryDeleteDto resultCountry = modelMapper.map(masterCountry, CountryDeleteDto.class);
        DefaultResponse<CountryDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultCountry);
        return response;
    }
}
