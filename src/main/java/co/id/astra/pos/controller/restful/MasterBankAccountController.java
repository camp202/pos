package co.id.astra.pos.controller.restful;


import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.MasterBankAccountDeleteDto;
import co.id.astra.pos.model.dto.MasterBankAccountEditDto;
import co.id.astra.pos.model.entity.master.MasterBankAccount;
import co.id.astra.pos.service.bankaccount.MasterBankAccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import co.id.astra.pos.model.dto.MasterBankAccountDto;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/masterBankAccount")
    public class MasterBankAccountController extends BaseController{
        @Autowired
        private MasterBankAccountService masterBankAccountService;
        @Autowired
        private ModelMapper modelMapper;

    @PostMapping
        public DefaultResponse save(@RequestBody MasterBankAccount masterBankAccount){
            masterBankAccountService.save(masterBankAccount);
            MasterBankAccountDto masterBankAccountDto=modelMapper.map(masterBankAccount,MasterBankAccountDto.class);
            DefaultResponse<MasterBankAccountDto> response = new DefaultResponse(Boolean.TRUE);
            response.setData(masterBankAccountDto);
            return response;
        }

    @GetMapping
    public DefaultResponse findAll() {
        List<MasterBankAccountDto> bankAccounts = masterBankAccountService.findAll().stream()
                .map(masterBankAccount -> modelMapper.map(masterBankAccount, MasterBankAccountDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<MasterBankAccountDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(bankAccounts);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody MasterBankAccountEditDto masterBankAccountEditDto) {
        MasterBankAccount masterBankAccountEdit = modelMapper.map(masterBankAccountEditDto, MasterBankAccount.class);
        MasterBankAccount masterBankAccount = masterBankAccountService.edit(masterBankAccountEdit);
        MasterBankAccountEditDto resultMasterBankAccount = modelMapper.map(masterBankAccount, MasterBankAccountEditDto.class);
        DefaultResponse<MasterBankAccountEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterBankAccount);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody MasterBankAccountDeleteDto masterBankAccountDeleteDto){
        MasterBankAccount masterBankAccountDelete = modelMapper.map(masterBankAccountDeleteDto,MasterBankAccount.class);
        MasterBankAccount masterBankAccount=masterBankAccountService.delete(masterBankAccountDelete);
        MasterBankAccountDeleteDto resultMasterBankAccount = modelMapper.map(masterBankAccount,MasterBankAccountDeleteDto.class);
        DefaultResponse<MasterBankAccountDeleteDto> response=new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterBankAccount);
        return response;
    }
}
