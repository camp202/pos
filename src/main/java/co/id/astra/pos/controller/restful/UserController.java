package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.UserDeleteDto;
import co.id.astra.pos.model.dto.UserDto;
import co.id.astra.pos.model.dto.UserEditDto;
import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.service.employee.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterUser user){
        userService.save(user);
        UserDto userDto = modelMapper.map(user, UserDto.class);
        DefaultResponse<UserDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(userDto);
        return response;
    }

    //karena restfull ga pertu /value lagi, karena yang membedakan adalah type mappingnya
    //jika ingin memakai type mapping yang sama maka di bedakan dengan parameternya
    //contoh @GetMapping(value=/{id})
    @GetMapping
    public DefaultResponse findAll(){
        List<UserDto> users = userService.findAll().stream()
                .map(user -> modelMapper.map(user, UserDto.class))
                .collect(Collectors.toList());

        DefaultResponse<List<UserDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(users);
        return response;
    }

    @GetMapping("/{roleId}")
    public DefaultResponse findByIdRole(@PathVariable("roleId") Long roleId){
        List<UserDto> result = userService.findAllByIdRole(roleId).stream()
                .map(branch -> modelMapper.map(branch, UserDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<UserDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody UserEditDto userEditDto){
        //userEditDTO adalah kolom2 yang diperbolehkan untuk diedit (kontraktor data)
        // di map dulu ke Class User
        MasterUser userEdit = modelMapper.map(userEditDto, MasterUser.class); //yang tadinya dari userDTO di mapping ke user lagi

        MasterUser user = userService.edit(userEdit);
        UserEditDto resultUser = modelMapper.map(user, UserEditDto.class);
        DefaultResponse<UserEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultUser);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody UserDeleteDto userDeleteDto){
        // di map dulu ke Class Master Employee
        MasterUser userDelete = modelMapper.map(userDeleteDto, MasterUser.class);

        MasterUser user = userService.delete(userDelete);
        UserDeleteDto resultUser = modelMapper.map(user, UserDeleteDto.class);
        DefaultResponse<UserDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultUser);
        return response;
    }
}
