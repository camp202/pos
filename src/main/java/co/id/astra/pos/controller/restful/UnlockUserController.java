package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.dto.UnlockUserDto;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.service.employee.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/unlock-user")
public class UnlockUserController extends BaseController {

    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    @PutMapping
    public DefaultResponse editLock(@RequestBody UnlockUserDto unlockUserDto) {
        // di map dulu ke Class User
        MasterUser unlockUser = modelMapper.map(unlockUserDto, MasterUser.class);
        MasterUser masterUser = userService.editLock(unlockUser);
        UnlockUserDto resultUser = modelMapper.map(masterUser, UnlockUserDto.class);
        DefaultResponse<UnlockUserDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultUser);
        return response;
    }
}
