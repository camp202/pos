package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.MDRDto;
import co.id.astra.pos.model.dto.MDREditDto;
import co.id.astra.pos.model.dto.MDRSaveDto;
import co.id.astra.pos.model.entity.master.MasterMDR;
import co.id.astra.pos.service.mdr.MDRService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/mdr")
public class MDRController extends BaseController {
    @Autowired
    private MDRService mdrService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterMDR masterMDR) {
        mdrService.save(masterMDR);
        MDRSaveDto mdrFormDto = modelMapper.map(masterMDR, MDRSaveDto.class);
        DefaultResponse<MDRSaveDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(mdrFormDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll() {
        List<MDRDto> masterMDRs = mdrService.findAll().stream()
                .map(masterMDR -> modelMapper.map(masterMDR, MDRDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<MDRDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(masterMDRs);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody MDREditDto mdrFormEditDto) {
        // di map dulu ke Class User
        MasterMDR mdrFormEdit = modelMapper.map(mdrFormEditDto, MasterMDR.class);

        MasterMDR masterMDR = mdrService.edit(mdrFormEdit);

        MDREditDto resultMDR = modelMapper.map(masterMDR, MDREditDto.class);
        DefaultResponse<MDREditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMDR);

        return response;
    }
}
