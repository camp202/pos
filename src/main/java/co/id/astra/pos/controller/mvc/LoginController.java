package co.id.astra.pos.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class LoginController {

    @GetMapping(value = "forgot-password")
    public String reset() { return "forgot-password"; }

    @GetMapping(value = "unlock-user")
    public String unlock() { return "unlock-user"; }
}