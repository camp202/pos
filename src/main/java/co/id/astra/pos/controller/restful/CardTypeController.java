package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.*;
import co.id.astra.pos.model.dto.CardTypeDeleteDto;
import co.id.astra.pos.model.dto.CardTypeDto;
import co.id.astra.pos.model.dto.CardTypeEditDto;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.entity.master.MasterCardType;
import co.id.astra.pos.service.cardtype.CardTypeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/cardtype")
public class CardTypeController extends BaseController {
    @Autowired
    private CardTypeService cardTypeService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    // post pasti untuk save kalau di restfull
    public DefaultResponse save(@RequestBody MasterCardType masterCardType){
        cardTypeService.save(masterCardType);
        CardTypeDto cardTypeDto = modelMapper.map(masterCardType, CardTypeDto.class);
        DefaultResponse<CardTypeDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(cardTypeDto);
        return response;
    }

    @GetMapping
    // get pasti untuk getdata kalau sudah di restfull
    public DefaultResponse findAll(){
        List<CardTypeDto> cardtypes = cardTypeService.findAll().stream()
                .map(masterCardType -> modelMapper.map(masterCardType, CardTypeDto.class))
                .collect(Collectors.toList());
        // userservice.findall --> dpt list user. lalu stream.map itu looping satu satu user (user) nyaa untuk di mapping ke modelMapper.
        // data use yg di mapper itu artinya mappingan user dr model ampper ke user dto. lalu collector.toList itu data data tadi di list lagi menjadi list user user yg sdh di looping tadii

        DefaultResponse<List<CardTypeDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(cardtypes);
        return response;
    }

    @PutMapping
    // put pasti untuk edit data kalau sudah restfull
    public DefaultResponse edit(@RequestBody CardTypeEditDto cardTypeEditDto){
        // di map dulu ke Class Outlet
        MasterCardType cardTypeEdit = modelMapper.map(cardTypeEditDto, MasterCardType.class);
        MasterCardType masterCardType = cardTypeService.edit(cardTypeEdit);
        CardTypeEditDto resultCardType = modelMapper.map(masterCardType, CardTypeEditDto.class);
        DefaultResponse<CardTypeEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultCardType);
        return response;
    }

    @DeleteMapping
    // delete pasti untuk delete kalau sdh di restfull
    public DefaultResponse delete(@RequestBody CardTypeDeleteDto cardTypeDeleteDto){
        // di map dulu ke Class User
        MasterCardType cardTypeDelete = modelMapper.map(cardTypeDeleteDto, MasterCardType.class);
        MasterCardType masterCardType = cardTypeService.delete(cardTypeDelete);
        CardTypeDeleteDto resultCardType = modelMapper.map(masterCardType, CardTypeDeleteDto.class);
        DefaultResponse<CardTypeDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultCardType);
        return response;
    }
}
