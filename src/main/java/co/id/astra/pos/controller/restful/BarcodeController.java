package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.BarcodeDeleteDto;
import co.id.astra.pos.model.dto.BarcodeDto;
import co.id.astra.pos.model.dto.BarcodeEditDto;
import co.id.astra.pos.model.entity.master.MasterBarcode;
import co.id.astra.pos.service.Barcode.BarcodeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/barcode")
public class BarcodeController {
    @Autowired
    private BarcodeService barcodeService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping   //Menyimpan data/mengisi data/Save/Create
    public DefaultResponse save(@RequestBody MasterBarcode barcode) {
        barcodeService.save(barcode);
        BarcodeDto barcodeDto = modelMapper.map(barcode, BarcodeDto.class);
        DefaultResponse<BarcodeDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(barcodeDto);
        return response;
    }

    @GetMapping    //mengambil data/Read
    public DefaultResponse findAll() {
        List<BarcodeDto> barcodes = barcodeService.findAll().stream()
                .map(barcode -> modelMapper.map(barcode, BarcodeDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<BarcodeDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(barcodes);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable ("id") Long id) {
        List<BarcodeDto> barcodes = barcodeService.findById(id).stream()
                .map(barcode -> modelMapper.map(barcode, BarcodeDto.class))
                .collect(Collectors.toList());
        DefaultResponse <List<BarcodeDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(barcodes);
        return response;
    }

    @PutMapping //mengedit data/mengupdate data ke data terbaru/Update
    public DefaultResponse edit(@RequestBody BarcodeEditDto barcodeEditDto){
        MasterBarcode barcodeEdit = modelMapper.map(barcodeEditDto, MasterBarcode.class);
        MasterBarcode barcode = barcodeService.edit(barcodeEdit);
        BarcodeEditDto resultMasterBarcode = modelMapper.map(barcode, BarcodeEditDto.class);
        DefaultResponse<BarcodeEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterBarcode);
        return response;
    }

    @DeleteMapping("/{id}")
    public DefaultResponse delete(@PathVariable ("id") Long id){
        // di map dulu ke Class MasterBarcode
        MasterBarcode barcode = barcodeService.deleteId(id);
        BarcodeDeleteDto resultMasterBarcode = modelMapper.map(barcode, BarcodeDeleteDto.class);
        DefaultResponse<BarcodeDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterBarcode);
        return response;
    }



}
