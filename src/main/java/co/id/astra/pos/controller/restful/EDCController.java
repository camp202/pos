package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.EDCDto;
import co.id.astra.pos.model.dto.EDCEditDto;
import co.id.astra.pos.model.dto.EDCSaveDto;
import co.id.astra.pos.model.entity.master.MasterEDC;
import co.id.astra.pos.service.edc.EDCService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/edc")
public class EDCController extends BaseController {
    @Autowired
    private EDCService edcService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterEDC masterEDC) {
        edcService.save(masterEDC);
        EDCSaveDto edcFormDto = modelMapper.map(masterEDC, EDCSaveDto.class);
        DefaultResponse<EDCSaveDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(edcFormDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll() {
        List<EDCDto> masterEDCs = edcService.findAll().stream()
                .map(masterEDC -> modelMapper.map(masterEDC, EDCDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<EDCDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(masterEDCs);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody EDCEditDto edcFormEditDto) {
        // di map dulu ke Class User
        MasterEDC edcFormEdit = modelMapper.map(edcFormEditDto, MasterEDC.class);

        MasterEDC masterEDC = edcService.edit(edcFormEdit);

        EDCEditDto resultEDC = modelMapper.map(masterEDC, EDCEditDto.class);
        DefaultResponse<EDCEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultEDC);

        return response;
    }
}
