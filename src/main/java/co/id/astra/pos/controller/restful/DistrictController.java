package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.DistrictDeleteDto;
import co.id.astra.pos.model.dto.DistrictDto;
import co.id.astra.pos.model.dto.DistrictEditDto;
import co.id.astra.pos.model.entity.master.MasterDistrict;
import co.id.astra.pos.service.district.DistrictService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/district")
public class DistrictController {
    @Autowired
    private DistrictService districtService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping   //Menyimpan data/mengisi data/Save/Create
    public DefaultResponse save(@RequestBody MasterDistrict district) {
        districtService.save(district);
        DistrictDto districtDto = modelMapper.map(district, DistrictDto.class);
        DefaultResponse<DistrictDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(districtDto);
        return response;
    }

    @GetMapping    //mengambil data/Read
    public DefaultResponse findAll() {
        List<DistrictDto> districts = districtService.findAll().stream()
                .map(district -> modelMapper.map(district, DistrictDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<DistrictDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(districts);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable("id") Long id) {
        List<DistrictDto> districts = districtService.getById(id).stream()
                .map(district -> modelMapper.map(district, DistrictDto.class))
                .collect(Collectors.toList());
        DefaultResponse <List<DistrictDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(districts);
        return response;
    }

    @PutMapping //mengedit data/mengupdate data ke data terbaru/Update
    public DefaultResponse edit(@RequestBody DistrictEditDto districtEditDto){
        MasterDistrict districtEdit = modelMapper.map(districtEditDto, MasterDistrict.class);
        MasterDistrict district = districtService.edit(districtEdit);
        DistrictEditDto resultMasterDistrict = modelMapper.map(district, DistrictEditDto.class);
        DefaultResponse<DistrictEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterDistrict);
        return response;
    }

//    @DeleteMapping("/{id}")
//    public DefaultResponse delete(@PathVariable ("id") Long id){
        // di map dulu ke Class MasterDistrict
//        MasterDistrict district = districtService.deleteId(id);
//        DistrictDeleteDto resultMasterDistrict = modelMapper.map(district, DistrictDeleteDto.class);
//        DefaultResponse<DistrictDeleteDto> response = new DefaultResponse(Boolean.TRUE);
//        response.setData(resultMasterDistrict);
//        return response;
//    }



}
