package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.ChangePassDto;
import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.service.changepass.ChangePassService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/changepass")
public class ChangePassController extends BaseController {
    @Autowired
    private ChangePassService changePassService;
    @Autowired
    private ModelMapper modelMapper;



    @PutMapping
    public DefaultResponse edit(@RequestBody ChangePassDto changePassDto){
        MasterUser user = modelMapper.map(changePassDto, MasterUser.class);
        MasterUser masterUser = changePassService.edit(user);
        ChangePassDto result = modelMapper.map(masterUser, ChangePassDto.class);
        DefaultResponse<ChangePassDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }
}