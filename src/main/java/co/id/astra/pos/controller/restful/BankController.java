package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.BankDeleteDto;
import co.id.astra.pos.model.dto.BankDto;
import co.id.astra.pos.model.dto.BankEditDto;
import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.service.bank.BankService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/bank")
public class BankController extends BaseController {
    @Autowired
    private BankService bankService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save (@RequestBody MasterBank masterBank){
        bankService.save(masterBank);
        BankDto bankDto = modelMapper.map(masterBank, BankDto.class);
        DefaultResponse<BankDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(bankDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<BankDto> banks = bankService.findAll().stream()
                .map(masterBank -> modelMapper.map(masterBank, BankDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<BankDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(banks);
        return response;
    }

    @GetMapping("{id}")
    public DefaultResponse getById(@PathVariable("id") Long id){
        List<BankEditDto> banks = bankService.getById(id).stream()
                .map(masterBank -> modelMapper.map(masterBank,BankEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<BankEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(banks);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody BankEditDto bankEditDto){
       MasterBank bankEdit = modelMapper.map(bankEditDto, MasterBank.class);
       MasterBank masterBank = bankService.edit(bankEdit);
       BankEditDto resultBank = modelMapper.map(masterBank, BankEditDto.class);
       DefaultResponse<BankEditDto> response = new DefaultResponse(Boolean.TRUE);
       response.setData(resultBank);
       return response;
    }

    @DeleteMapping
    public DefaultResponse delete (@RequestBody BankDeleteDto bankDeleteDto){
        MasterBank bankDelete = modelMapper.map(bankDeleteDto, MasterBank.class);
        MasterBank masterBank = bankService.delete(bankDelete);
        BankDeleteDto resultBank = modelMapper.map(masterBank, BankDeleteDto.class);
        DefaultResponse<BankDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultBank);
        return response;
    }
}
