package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.*;
import co.id.astra.pos.model.entity.master.MasterItem;
import co.id.astra.pos.model.entity.master.MasterItemVariant;
import co.id.astra.pos.repository.ItemInvRepository;
import co.id.astra.pos.repository.ItemVarRepository;
import co.id.astra.pos.service.Item.ItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/item")
public class ItemController {
    @Autowired
    private ItemService itemService;
    @Autowired
    private ItemVarRepository itemVarRepository;
    @Autowired
    private ItemInvRepository itemInvRepository;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save (@RequestBody ItemDto item){
        itemService.save(item);
        DefaultResponse<ItemDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(item);
        return response;
    }

    @GetMapping
    public DefaultResponse findAllItem() {
        List<ItemGetVarDto> items = itemService.findAllItem().stream()
                .map(item -> modelMapper.map(item, ItemGetVarDto.class))
                .collect(Collectors.toList());
        for (ItemGetVarDto itemGetVar : items) {
            MasterItem item = new MasterItem();
            item.setId(itemGetVar.getId());
            List<ItemVarGetInvDto> itemVars = itemVarRepository.findAllByMasterItem(item).stream()
                    .map(masterItem -> modelMapper.map(masterItem, ItemVarGetInvDto.class))
                    .collect(Collectors.toList());
            for (ItemVarGetInvDto itemVarGetInv: itemVars) {
                MasterItemVariant itemVar = new MasterItemVariant();
                itemVar.setId(itemVarGetInv.getId());
                List<ItemInvGetDto> itemInvs = itemInvRepository.findAllByMasterItemVariant(itemVar).stream()
                        .map(masterItemVar -> modelMapper.map(masterItemVar, ItemInvGetDto.class))
                        .collect(Collectors.toList());
                itemVarGetInv.setItemInventories(itemInvs);
            }
            itemGetVar.setItemVariants(itemVars);
        }
        DefaultResponse<List<ItemGetVarDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(items);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody ItemDto item){
        itemService.edit(item);
        DefaultResponse<ItemDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(item);
        return response;
    }
}
