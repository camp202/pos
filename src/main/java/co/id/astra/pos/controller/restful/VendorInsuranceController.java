package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.VendorInsuranceDeleteDto;
import co.id.astra.pos.model.dto.VendorInsuranceDto;
import co.id.astra.pos.model.dto.VendorInsuranceEditDto;
import co.id.astra.pos.model.entity.master.MasterInsurance;
import co.id.astra.pos.service.insurance.VendorInsuranceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/insurance")
public class VendorInsuranceController extends BaseController {
    @Autowired
    private VendorInsuranceService insuranceService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save (@RequestBody MasterInsurance masterInsurance){
        insuranceService.save(masterInsurance);
        VendorInsuranceDto insuranceDto = modelMapper.map(masterInsurance, VendorInsuranceDto.class);
        DefaultResponse<VendorInsuranceDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(insuranceDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<VendorInsuranceDto> insurances = insuranceService.findAll().stream()
                .map(masterInsurance -> modelMapper.map(masterInsurance, VendorInsuranceDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorInsuranceDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(insurances);
        return response;
    }

    @GetMapping("{id}")
    public DefaultResponse getById(@PathVariable("id") Long id){
        List<VendorInsuranceEditDto> insurances = insuranceService.findById(id).stream()
                .map(masterInsurance -> modelMapper.map(masterInsurance,VendorInsuranceEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorInsuranceEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(insurances);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody VendorInsuranceEditDto insuranceEditDto){
        MasterInsurance insuranceEdit = modelMapper.map(insuranceEditDto, MasterInsurance.class);
        MasterInsurance masterInsurance = insuranceService.edit(insuranceEdit);
        VendorInsuranceEditDto resultInsurance = modelMapper.map(masterInsurance, VendorInsuranceEditDto.class);
        DefaultResponse<VendorInsuranceEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultInsurance);
        return response;
    }

    @DeleteMapping("/{id}")
    public DefaultResponse deleteId (@PathVariable("id") Long id){
        MasterInsurance insurance = insuranceService.deleteId(id);
        VendorInsuranceDeleteDto resultInsurance = modelMapper.map(insurance, VendorInsuranceDeleteDto.class);
        DefaultResponse<VendorInsuranceDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultInsurance);
        return response;
    }

}
