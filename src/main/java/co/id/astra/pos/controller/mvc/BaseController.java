package co.id.astra.pos.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class BaseController {
    @GetMapping("supplier")
    public String supplier() {
        return "supplier";
    }
    @GetMapping("role")
    public String role() {
        return "role";
    }
    @GetMapping("paymentmethod")
    public String paymentmethod(){return "paymentmethod";}
    @GetMapping("bankaccount")
    public String bankaccount(){return "bankaccount";}

    @GetMapping(value = "admin")
    public String admin() {
        return "admin";
    }

    @GetMapping("barcode")
    public String barcode() {
        return "barcode";
    }

    @GetMapping("customer")
    public String customer() {
        return "customer";
    }

    @GetMapping("coutlet")
    public String coutlet() {
        return "chooseoutletview";
    }

    @GetMapping("district")
    public  String district() {
        return "districtview";
    }

    @GetMapping("vendortvcable")
    public  String tvcable() {
        return "vendortvcableview";
    }

    @GetMapping(value = "user")
    public String index() {
        return "userForm";
    }

    @GetMapping("employee")
    public String viewFormEmployee() {
        return "employee";
    }

    @GetMapping("userForm")
    public String viewFormUser() {
        return "userForm";
    }

    @GetMapping("test")
    public String test(){ return "test"; }

    @GetMapping("bank")
    public String bank() {return "bank1";}

    @GetMapping("country")
    public String country(){return "country1";}

    @GetMapping("item")
    public String item(){ return "item"; }

    @GetMapping("region")
    public String region(){ return "region"; }

    @GetMapping("province")
    public String province(){ return "province"; }

    @GetMapping("insurance")
    public String insurance(){return "insurance";}

    @GetMapping(value = "category")
    public String category() {return "category";}

    @GetMapping(value = "outlet")
    public String outlet() {
        return "outlet";
    }

    @GetMapping(value = "cardtype")
    public String cardtype() {
        return "cardtype";
    }

    @GetMapping(value = "dashboard")
    public String dashboard() {
        return "dashboard";
    }
    @GetMapping("installment")
    public String installment() {return "installment";}
    @GetMapping("login")
    public String login() {
        return "login";
    }

    @GetMapping("changepassword")
    public String changepassword() {
        return "changepassword";
    }

    @GetMapping("vendorInternet")
    public String vendorInternet() {
        return "vendorInternet";
    }

    @GetMapping("vendor")
    public String vendor() {
        return "vendor";
    }

}
