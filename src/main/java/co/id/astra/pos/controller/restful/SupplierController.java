package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.dto.SupplierDeleteDto;
import co.id.astra.pos.model.dto.SupplierEditDto;
import co.id.astra.pos.model.entity.master.MasterSupplier;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.SupplierDto;
import co.id.astra.pos.service.supplier.SupplierService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping   //Menyimpan data/mengisi data/Save/Create
    public DefaultResponse save(@RequestBody MasterSupplier supplier) {
        supplierService.save(supplier);
        SupplierDto supplierDto = modelMapper.map(supplier, SupplierDto.class);
        DefaultResponse<SupplierDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(supplierDto);
        return response;
    }

    @GetMapping    //mengambil data/Read
    public DefaultResponse findAll() {
        List<SupplierDto> suppliers = supplierService.findAll().stream()
                .map(supplier -> modelMapper.map(supplier, SupplierDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<SupplierDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(suppliers);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable ("id") Long id) {
        List<SupplierDto> suppliers = supplierService.findById(id).stream()
                .map(supplier -> modelMapper.map(supplier, SupplierDto.class))
                .collect(Collectors.toList());
        DefaultResponse <List<SupplierDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(suppliers);
        return response;
    }

    @PutMapping //mengedit data/mengupdate data ke data terbaru/Update
    public DefaultResponse edit(@RequestBody SupplierEditDto supplierEditDto){
        MasterSupplier supplierEdit = modelMapper.map(supplierEditDto, MasterSupplier.class);
        MasterSupplier supplier = supplierService.edit(supplierEdit);
        SupplierEditDto resultMasterSupplier = modelMapper.map(supplier, SupplierEditDto.class);
        DefaultResponse<SupplierEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterSupplier);
        return response;
    }

    @DeleteMapping("/{id}")
    public DefaultResponse delete(@PathVariable ("id") Long id){
        // di map dulu ke Class MasterSupplier
        MasterSupplier supplier = supplierService.deleteId(id);
        SupplierDeleteDto resultMasterSupplier = modelMapper.map(supplier, SupplierDeleteDto.class);
        DefaultResponse<SupplierDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterSupplier);
        return response;
    }



}
