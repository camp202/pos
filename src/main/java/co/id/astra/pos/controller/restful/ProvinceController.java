package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.*;
import co.id.astra.pos.model.entity.master.MasterProvince;
import co.id.astra.pos.service.province.ProvinceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/province")
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save (@RequestBody MasterProvince province){
        provinceService.save(province);
        ProvinceSaveDto provinceDto = modelMapper.map(province, ProvinceSaveDto.class);
        DefaultResponse<ProvinceSaveDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(provinceDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<ProvinceGetDto> provinces = provinceService.findAll().stream()
                .map(province -> modelMapper.map(province, ProvinceGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<ProvinceGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(provinces);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable("id") Long id){
        List<ProvinceGetDto> provinces = provinceService.findById(id).stream()
                .map(province -> modelMapper.map(province, ProvinceGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<ProvinceGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(provinces);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody ProvinceEditDto provinceDto){
        MasterProvince provinceEdit = modelMapper.map(provinceDto, MasterProvince.class);

        MasterProvince province = provinceService.edit(provinceEdit);
        ProvinceEditDto resultProvince = modelMapper.map(province, ProvinceEditDto.class);
        DefaultResponse<ProvinceEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultProvince);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody ProvinceDeleteDto provinceDto){
        MasterProvince provinceDelete = modelMapper.map(provinceDto, MasterProvince.class);

        MasterProvince province = provinceService.delete(provinceDelete);
        ProvinceDeleteDto resultProvince = modelMapper.map(province, ProvinceDeleteDto.class);
        DefaultResponse<ProvinceDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultProvince);
        return response;
    }
}
