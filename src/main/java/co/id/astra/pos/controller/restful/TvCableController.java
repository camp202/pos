package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.tvcable.VendorTvCableEditDto;
import co.id.astra.pos.model.dto.tvcable.VendorTvCableGetDto;
import co.id.astra.pos.model.dto.tvcable.VendorTvCableSaveDto;
import co.id.astra.pos.model.entity.master.MasterVendorTvCable;
import co.id.astra.pos.service.tvcable.TvCableService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/vendor/tvcable")
public class TvCableController {
    @Autowired
    private TvCableService tvCableService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public DefaultResponse findAllActive(){
        List<VendorTvCableGetDto> vendorTvCables = tvCableService.findAllActive().stream()
                .map(tv -> modelMapper.map(tv, VendorTvCableGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorTvCableGetDto>> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(vendorTvCables);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable("id") Long id) {
        List<VendorTvCableGetDto> vendorTvCableGetDtos = tvCableService.findById(id).stream()
                .map(tv -> modelMapper.map(tv, VendorTvCableGetDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorTvCableGetDto>> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(vendorTvCableGetDtos);
        return response;
    }

    @PostMapping
    public DefaultResponse save(@RequestBody MasterVendorTvCable masterVendorTvCable) {
        tvCableService.save(masterVendorTvCable);
        VendorTvCableSaveDto tvcable = modelMapper.map(masterVendorTvCable, VendorTvCableSaveDto.class);
        DefaultResponse<VendorTvCableSaveDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(tvcable);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody VendorTvCableEditDto vendorTvCableGetDto) {
        MasterVendorTvCable vendorTvCableEdit = modelMapper.map(vendorTvCableGetDto, MasterVendorTvCable.class);
        MasterVendorTvCable vendorTvCable = tvCableService.edit(vendorTvCableEdit);
        VendorTvCableEditDto res = modelMapper.map(vendorTvCable, VendorTvCableEditDto.class);
        DefaultResponse<VendorTvCableEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(res);
        return response;
    }

    @DeleteMapping("{id}")
    public DefaultResponse delete(@PathVariable("id") Long id) {
        MasterVendorTvCable masterVendorTvCable = tvCableService.delete(id);
        VendorTvCableGetDto deleteResult = modelMapper.map(masterVendorTvCable, VendorTvCableGetDto.class);
        DefaultResponse<VendorTvCableGetDto> response = new DefaultResponse<>(Boolean.TRUE);
        response.setData(deleteResult);
        return response;
    }

}
