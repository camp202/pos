package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.VendorInternetDeleteDto;
import co.id.astra.pos.model.dto.VendorInternetDto;
import co.id.astra.pos.model.dto.VendorInternetEditDto;
import co.id.astra.pos.model.dto.VendorInternetGetDto;
import co.id.astra.pos.model.entity.master.MasterVendorInternet;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/vendorInternet")
public class VendorInternetController extends BaseController {
    @Autowired
    private co.id.astra.pos.service.internet.VendorInternetService vendorInternetService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    // post pasti untuk save kalau di restfull
    public DefaultResponse save(@RequestBody MasterVendorInternet masterVendorInternet){
        vendorInternetService.save(masterVendorInternet);
        VendorInternetDto vendorInternetDto = modelMapper.map(masterVendorInternet, VendorInternetDto.class);
        DefaultResponse<VendorInternetDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(vendorInternetDto);
        return response;
    }

    @GetMapping("{id}")
    // get pasti untuk getdata kalau sudah di restfull
    public DefaultResponse getById(@PathVariable ("id") Long id){
        List<VendorInternetEditDto> vendors = vendorInternetService.findById(id).stream()
                .map(masterVendorInternet -> modelMapper.map(masterVendorInternet, VendorInternetEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorInternetEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(vendors);
        return response;
    }


    @GetMapping
    // get pasti untuk get data kalau sudah di restfull
    public DefaultResponse findAll(){
        List<VendorInternetGetDto> vendors = vendorInternetService.findAll().stream()
                .map(masterVendorInternet -> modelMapper.map(masterVendorInternet, VendorInternetGetDto.class))
                .collect(Collectors.toList());
        // userservice.findall --> dpt list user. lalu stream.map itu looping satu satu user (user) nyaa untuk di mapping ke modelMapper.
        // data use yg di mapper itu artinya mappingan user dr model mapper ke user dto. lalu collector.toList itu data data tadi di list lagi menjadi list user user yg sdh di looping tadii

        DefaultResponse<List<VendorInternetGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(vendors);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody VendorInternetDto vendorInternetDto){
        MasterVendorInternet vendorEdit = modelMapper.map(vendorInternetDto, MasterVendorInternet.class);
        MasterVendorInternet masterVendorInternet = vendorInternetService.edit(vendorEdit);
        VendorInternetDto resultVendor = modelMapper.map(masterVendorInternet, VendorInternetDto.class);
        DefaultResponse<VendorInternetDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultVendor);
        return response;
    }

//    @DeleteMapping()
//    public DefaultResponse delete(@RequestBody VendorInternetDeleteDto vendorInternetDeleteDto) {
//        MasterVendorInternet masterVendorInternetDelete = modelMapper.map(vendorInternetDeleteDto, MasterVendorInternet.class);
//
//        MasterVendorInternet masterVendorInternet = vendorInternetService.delete(masterVendorInternetDelete);
//        VendorInternetDeleteDto resultVendorInternet = modelMapper.map(masterVendorInternet, VendorInternetDeleteDto.class);
//        DefaultResponse<VendorInternetController> response = new DefaultResponse<>(Boolean.TRUE);
//        response.setData(resultVendorInternet);
//        return response;
//    }

    @DeleteMapping("/{id}")
    public DefaultResponse deleteId(@PathVariable("id") Long id){
        MasterVendorInternet leasing = vendorInternetService.deleteId(id);
        VendorInternetDeleteDto resultVendor = modelMapper.map(leasing, VendorInternetDeleteDto.class);
        DefaultResponse<VendorInternetDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultVendor);
        return response;
    }

}
