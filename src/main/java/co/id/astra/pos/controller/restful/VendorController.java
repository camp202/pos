package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.VendorDeleteDto;
import co.id.astra.pos.model.dto.VendorDto;
import co.id.astra.pos.model.dto.VendorEditDto;
import co.id.astra.pos.model.entity.master.MasterVendor;
import co.id.astra.pos.service.vendor.VendorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/vendor")
public class VendorController extends BaseController {
    @Autowired
    private VendorService vendorService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterVendor masterVendor) {
        vendorService.save(masterVendor);
        VendorDto vendorDto = modelMapper.map(masterVendor, VendorDto.class);
        DefaultResponse<VendorDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(vendorDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<VendorDto> vendors = vendorService.findAll().stream()
                .map(masterVendor -> modelMapper.map(masterVendor,VendorDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorDto>> response=new DefaultResponse(Boolean.TRUE);
        response.setData(vendors);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse getById(@PathVariable("id") Long id){
        List<VendorEditDto>vendors = vendorService.getById(id).stream()
                .map(masterVendor -> modelMapper.map(masterVendor,VendorEditDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<VendorEditDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(vendors);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody VendorEditDto vendorEditDto){
        MasterVendor vendorEdit = modelMapper.map(vendorEditDto,MasterVendor.class);
        MasterVendor masterVendor=vendorService.edit(vendorEdit);
        VendorEditDto resulteditvendor = modelMapper.map(masterVendor,VendorEditDto.class);
        DefaultResponse<VendorEditDto> response=new DefaultResponse(Boolean.TRUE);
        response.setData(resulteditvendor);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody VendorDeleteDto vendorDeleteDto){
        MasterVendor vendordelete = modelMapper.map(vendorDeleteDto, MasterVendor.class);
        MasterVendor masterVendor = vendorService.delete(vendordelete);
        VendorDeleteDto resultVendor = modelMapper.map(masterVendor,VendorDeleteDto.class);
        DefaultResponse<VendorDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultVendor);
        return response;
    }

}
