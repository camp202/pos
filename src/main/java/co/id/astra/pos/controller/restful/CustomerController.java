package co.id.astra.pos.controller.restful;


import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.CustomerDto;
import co.id.astra.pos.model.dto.CustomerEditDto;
import co.id.astra.pos.model.dto.CustomerSaveDto;
import co.id.astra.pos.model.entity.master.MasterCustomer;
import co.id.astra.pos.service.customer.CustomerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/customer")
public class CustomerController extends BaseController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterCustomer masterCustomer) {
        customerService.save(masterCustomer);
        CustomerSaveDto customerFormDto = modelMapper.map(masterCustomer, CustomerSaveDto.class);
        DefaultResponse<CustomerSaveDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(customerFormDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll() {
        List<CustomerDto> masterCustomers= customerService.findAll().stream()
                .map(masterCustomer -> modelMapper.map(masterCustomer, CustomerDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<CustomerDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(masterCustomers);
        System.out.println("FindAll : "+masterCustomers.toString());
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody CustomerEditDto customerFormEditDto){
        // di map dulu ke Class User
        MasterCustomer customerFormEdit = modelMapper.map(customerFormEditDto, MasterCustomer.class);

        MasterCustomer masterCustomer = customerService.edit(customerFormEdit);

        CustomerEditDto resultCustomer = modelMapper.map(masterCustomer, CustomerEditDto.class);
        DefaultResponse<CustomerEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultCustomer);

        return response;
    }

}
