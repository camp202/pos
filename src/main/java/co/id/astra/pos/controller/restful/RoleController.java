package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.RoleDto;
import co.id.astra.pos.model.dto.RoleEditDto;
import co.id.astra.pos.model.entity.master.MasterRole;
import co.id.astra.pos.service.role.RoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/role")
public class RoleController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public DefaultResponse save(@RequestBody MasterRole role) {
        roleService.save(role);
        RoleDto roleDto = modelMapper.map(role, RoleDto.class);
        DefaultResponse<RoleDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(roleDto);
        return response;
    }

     @GetMapping
     public DefaultResponse get() {
         List<RoleDto> roles = roleService.findAll().stream()
            .map(role -> modelMapper.map(role, RoleDto.class))
            .collect(Collectors.toList());
         DefaultResponse<List<RoleDto>> response = new DefaultResponse(Boolean.TRUE);
         response.setData(roles);
         return response;
        }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable ("id") Long id) {
        List<RoleDto> roles = roleService.findById(id).stream()
                .map(role -> modelMapper.map(role, RoleDto.class))
                .collect(Collectors.toList());
        DefaultResponse <List<RoleDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(roles);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody RoleEditDto roleEditDto) {
        MasterRole roleEdit = modelMapper.map(roleEditDto, MasterRole.class);
        MasterRole role = roleService.edit(roleEdit);
        RoleEditDto resultMasterRole = modelMapper.map(role, RoleEditDto.class);
        DefaultResponse<RoleEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterRole);
        return response;
    }
}
