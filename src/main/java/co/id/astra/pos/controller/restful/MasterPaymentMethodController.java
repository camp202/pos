package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.MasterPaymentMethodDeleteDto;
import co.id.astra.pos.model.dto.MasterPaymentMethodDto;
import co.id.astra.pos.model.entity.master.MasterPaymentMethod;
import co.id.astra.pos.service.paymentmethod.MasterPaymentMethodService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import co.id.astra.pos.model.dto.MasterPaymentMethodEditDto;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/masterPaymentMethod")
public class MasterPaymentMethodController extends BaseController {
    @Autowired
    private MasterPaymentMethodService masterPaymentMethodService;
    @Autowired
    private ModelMapper modalMapper;

    @PostMapping
        public DefaultResponse save(@RequestBody MasterPaymentMethod masterPaymentMethod){
        masterPaymentMethodService.save(masterPaymentMethod);
        MasterPaymentMethodDto masterPaymentMethodDto=modalMapper.map(masterPaymentMethod,MasterPaymentMethodDto.class);
        DefaultResponse<MasterPaymentMethodDto>response = new DefaultResponse(Boolean.TRUE);
        response.setData(masterPaymentMethodDto);
        return response;
    }

    @GetMapping
    public DefaultResponse findAll(){
        List<MasterPaymentMethodDto> paymentMethods = masterPaymentMethodService.findAll().stream()
                .map(masterPaymentMethod -> modalMapper.map(masterPaymentMethod,MasterPaymentMethodDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<MasterPaymentMethodDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(paymentMethods);
        return response;
    }

    @GetMapping("/{id}")
    public DefaultResponse findById(@PathVariable ("id") Long id){
        List<MasterPaymentMethodDto> paymentMethods = masterPaymentMethodService.findById(id).stream()
                .map(masterPaymentMethod -> modalMapper.map(masterPaymentMethod,MasterPaymentMethodDto.class))
                .collect(Collectors.toList());
        DefaultResponse<List<MasterPaymentMethodDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(paymentMethods);
        return response;
    }



    @PutMapping
    public DefaultResponse edit(@RequestBody MasterPaymentMethodEditDto masterPaymentMethodEditDto){
        MasterPaymentMethod masterPaymentMethodEdit = modalMapper.map(masterPaymentMethodEditDto, MasterPaymentMethod.class);
        MasterPaymentMethod masterPaymentMethod = masterPaymentMethodService.edit(masterPaymentMethodEdit);
        MasterPaymentMethodEditDto resultMasterPaymentMethod = modalMapper.map(masterPaymentMethod, MasterPaymentMethodEditDto.class);
        DefaultResponse<MasterPaymentMethodEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterPaymentMethod);
        return response;
    }

    @DeleteMapping
    public DefaultResponse delete(@RequestBody MasterPaymentMethodDeleteDto masterPaymentMethodDeleteDto){
        MasterPaymentMethod masterPaymentMethodDelete = modalMapper.map(masterPaymentMethodDeleteDto,MasterPaymentMethod.class);
        MasterPaymentMethod masterPaymentMethod = masterPaymentMethodService.delete(masterPaymentMethodDelete);
        MasterPaymentMethodDeleteDto resultMasterPaymentMethod = modalMapper.map(masterPaymentMethod, MasterPaymentMethodDeleteDto.class);
        DefaultResponse<MasterPaymentMethodDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultMasterPaymentMethod);
        return response;
    }
}
