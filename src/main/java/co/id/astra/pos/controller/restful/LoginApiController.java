package co.id.astra.pos.controller.restful;

import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.model.dto.LoginDto;
import co.id.astra.pos.model.dto.LoginShowDto;
import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.service.employee.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
public class LoginApiController {
    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping
    public DefaultResponse login(@RequestBody LoginDto loginDto) {
        MasterUser masterUser = userService.findByUsername(loginDto.getUsername());
        Boolean isLogin = passwordEncoder.matches(loginDto.getPassword(), masterUser.getPassword());

        DefaultResponse<LoginShowDto> response = new DefaultResponse(Boolean.TRUE);

        if (isLogin) {
            LoginShowDto result = modelMapper.map(masterUser, LoginShowDto.class);

            response.setData(result);

        } else {
            response.setMessage("can not login");
            response.setStatus("01");
        }


        return response;
    }
}
