package co.id.astra.pos.controller.restful;

import co.id.astra.pos.controller.mvc.BaseController;
import co.id.astra.pos.model.dto.OutletDeleteDto;
import co.id.astra.pos.model.dto.OutletDto;
import co.id.astra.pos.model.dto.OutletEditDto;
import co.id.astra.pos.model.dto.OutletGetDto;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import co.id.astra.pos.model.entity.DefaultResponse;
import co.id.astra.pos.service.outlet.OutletService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/outlet")
public class OutletController extends BaseController {
    @Autowired
    private OutletService outletService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    // post pasti untuk save kalau di restfull
    public DefaultResponse save(@RequestBody MasterOutlet masterOutlet){
        outletService.save(masterOutlet);
        OutletDto outletDto = modelMapper.map(masterOutlet, OutletDto.class);
        DefaultResponse<OutletDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(outletDto);
        return response;
    }

    @GetMapping
    // get pasti untuk getdata kalau sudah di restfull
    public DefaultResponse findAll(){
        List<OutletGetDto> outlets = outletService.findAll().stream()
                .map(masterOutlet -> modelMapper.map(masterOutlet, OutletGetDto.class))
                .collect(Collectors.toList());
        // userservice.findall --> dpt list user. lalu stream.map itu looping satu satu user (user) nyaa untuk di mapping ke modelMapper.
        // data use yg di mapper itu artinya mappingan user dr model ampper ke user dto. lalu collector.toList itu data data tadi di list lagi menjadi list user user yg sdh di looping tadii

        DefaultResponse<List<OutletGetDto>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(outlets);
        return response;
    }

    @PutMapping
    // put pasti untuk edit data kalau sudah restfull
    public DefaultResponse edit(@RequestBody OutletEditDto outletEditDto){
        // di map dulu ke Class Ouutlet
        MasterOutlet outletEdit = modelMapper.map(outletEditDto, MasterOutlet.class);
        MasterOutlet masterOutlet = outletService.edit(outletEdit);
        OutletEditDto resultOutlet = modelMapper.map(masterOutlet, OutletEditDto.class);
        DefaultResponse<OutletEditDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultOutlet);
        return response;
    }

    @DeleteMapping
    // delete pasti untuk delete kalau sdh di restfull
    public DefaultResponse delete(@RequestBody OutletDeleteDto outletDeleteDto){
        // di map dulu ke Class User
        MasterOutlet outletDelete = modelMapper.map(outletDeleteDto, MasterOutlet.class);
        MasterOutlet masterOutlet = outletService.delete(outletDelete);
        OutletDeleteDto resultOutlet = modelMapper.map(masterOutlet, OutletDeleteDto.class);
        DefaultResponse<OutletDeleteDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(resultOutlet);
        return response;
    }
}
