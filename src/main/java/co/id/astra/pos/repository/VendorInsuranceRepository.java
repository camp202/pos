package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterInsurance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorInsuranceRepository extends JpaRepository<MasterInsurance, String> {
    List<MasterInsurance> findById (Long id);
    @Query ("select i from MasterInsurance i where i.active = true")
    List<MasterInsurance> findAllDataActive();
    MasterInsurance getById(Long id);


}
