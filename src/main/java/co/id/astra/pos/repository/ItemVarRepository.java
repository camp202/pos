package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterItem;
import co.id.astra.pos.model.entity.master.MasterItemVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemVarRepository extends JpaRepository<MasterItemVariant, String > {

    MasterItemVariant findById(Long id);

    MasterItemVariant findByName(String name);

    List<MasterItemVariant> findAllByMasterItem(MasterItem item);

    @Query("select iv from MasterItemVariant iv where iv.active = true")
    List<MasterItemVariant> findAllDataActive();
}
