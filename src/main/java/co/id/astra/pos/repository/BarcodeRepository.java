package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterBarcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BarcodeRepository extends JpaRepository<MasterBarcode, String> {
    MasterBarcode getById(Long id);

    @Query("select b from MasterBarcode b where b.active = true")
    List<MasterBarcode> findAllDataActive();

    @Query("SELECT mb FROM MasterBarcode mb WHERE mb.id = :id AND mb.active = true")
    List<MasterBarcode> findById(Long id);
}
