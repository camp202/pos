package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterOutlet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OutletRepository extends JpaRepository<MasterOutlet, String> {
    @Query("SELECT o FROM MasterOutlet o WHERE o.id = :id")
    List<MasterOutlet> getById(Long id);

    MasterOutlet findById(Long Id);

    @Query("select u from MasterOutlet u where u.active = true")
    List<MasterOutlet> findAllDataActive();


}
