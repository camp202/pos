package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DistrictRepository extends JpaRepository<MasterDistrict, String> {
//    @Query("SELECT d FROM MasterDistrict d WHERE d.masterRegion = :id")
//    List<MasterDistrict> getByIdRegion(Long id);

    @Query("SELECT d FROM MasterDistrict d WHERE d.id = :id")
    List<MasterDistrict> getById(Long id);

    @Query("SELECT d.id FROM MasterDistrict d WHERE d.id = :id")
    MasterDistrict editById(Long id);

    MasterDistrict findById(Long id);
}
