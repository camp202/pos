package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankRepository extends JpaRepository<MasterBank, String> {
    MasterBank findById (Long id);
    @Query("select b from MasterBank b where b.active = true")
    List<MasterBank> findAllDataActive();
    List<MasterBank> getById(Long id);

}
