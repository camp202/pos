package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<MasterUser, String> {
//    @Autowired
//    private MasterEmployee masterEmployee;

    MasterUser findById(Long id);

    @Query("select u from MasterUser u where u.active = true")
    List<MasterUser> findAllDataActive();

//
//    @Query("select u from MasterUser u inner join MasterEmployee e on u.employee_id = e.id where u.active = true ")
//    List<MasterEmployee> findAllDataActiveEmployee();

//    ======KENAPA GABISAAAAA???=======
//    List<MasterUser> findAllByIdRole(MasterRole masterRole);

    MasterUser findByUsername(String username);
}
