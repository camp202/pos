package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<MasterEmployee, String> {
    MasterEmployee findById(Long id);

    @Query("select u from MasterEmployee u where u.active = true")
    List<MasterEmployee> findAllDataActive();

    @Query("SELECT e FROM MasterEmployee e WHERE e.id = :id AND e.active = true")
    List<MasterEmployee> getById(Long id);
//    @Query("select u from MasterUser u where u. = u.)
//    List<MasterEmployee> findAllDataActiveEmp();

}
