package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterCategory;
import co.id.astra.pos.model.entity.master.MasterItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<MasterItem, String> {

    MasterItem findById(Long id);

    MasterItem findByName(String name);


    //    Mencari master Item berdasarkan id MasterCategory
//    @Query("select i.id from MasterItem i where i.active = TRUE")
    List<MasterItem> findAllByMasterCategory (MasterCategory masterCategory);

    @Query("select i from MasterItem i where i.active = true")
    List<MasterItem> findAllDataActive();
    List<MasterItem> findAllById (Long id);

}
