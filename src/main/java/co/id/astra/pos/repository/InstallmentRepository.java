package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterInstallment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstallmentRepository extends JpaRepository<MasterInstallment, String> {

    MasterInstallment findById (Long id);

    @Query("SELECT i from MasterInstallment i where i.id= :id")
    List<MasterInstallment> getById(Long id);

    @Query("select mi from MasterInstallment mi where mi.active = true")
    List<MasterInstallment>findAllDataActive();
}
