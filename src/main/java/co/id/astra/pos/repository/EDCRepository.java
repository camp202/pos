package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterEDC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EDCRepository extends JpaRepository<MasterEDC, String> {

    MasterEDC findById(Long id);

    @Query("select u from MasterCustomer u where u.active = true")
    List<MasterEDC> findAllDataActive();
}
