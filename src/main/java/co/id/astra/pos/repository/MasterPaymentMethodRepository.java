package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterPaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MasterPaymentMethodRepository extends JpaRepository <MasterPaymentMethod, String> {
    List<MasterPaymentMethod> findById(Long id);
    MasterPaymentMethod getById(Long id);
    @Query("select pm from MasterPaymentMethod pm where pm.active=true")
    List<MasterPaymentMethod>findAllDataActive();
}
