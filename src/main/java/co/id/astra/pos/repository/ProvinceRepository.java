package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.model.entity.master.MasterProvince;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProvinceRepository extends JpaRepository<MasterProvince, String> {
    MasterProvince findById(Long id);

    MasterProvince findByName(String name);

    List<MasterProvince> findAllById(Long id);

    @Query("select p from MasterProvince p where p.active = true")
    List<MasterProvince> findAllDataActive();
}
