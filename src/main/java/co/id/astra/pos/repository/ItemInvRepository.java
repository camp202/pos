package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.ItemInventory;
import co.id.astra.pos.model.entity.master.MasterItemVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemInvRepository extends JpaRepository<ItemInventory, String > {

    ItemInventory findById(long id);

    List<ItemInventory> findAllByMasterItemVariant(MasterItemVariant itemVar);

    @Query("select i from ItemInventory i where i.active = true")
    List<ItemInventory> findAllDataActive();
}
