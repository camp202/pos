package co.id.astra.pos.repository;


import co.id.astra.pos.model.entity.master.MasterVendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorRepository extends JpaRepository<MasterVendor, String> {
    @Query("SELECT v FROM MasterVendor v WHERE v.id = :id")
    List<MasterVendor>getById(Long id);
    MasterVendor findById(Long id);
    @Query("SELECT mv from MasterVendor mv where mv.active=true")
    List<MasterVendor>findAllDataActive();
}
