package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<MasterRole, String> {
    List<MasterRole> findById(Long id);

    @Query("select r from MasterRole r where r.active=true")
    List<MasterRole> findAllDataActive();
    @Query("select mr from MasterRole mr where mr.id= :id and mr.active=true")
    MasterRole getById(Long id);
//    @Query("select ")
//    @Query("select r from MasterRole r where r.code= :code")
//    MasterRole save();

}
