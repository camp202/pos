package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterVendorTvCable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TvCableRepository extends JpaRepository<MasterVendorTvCable, String> {
    @Query("SELECT tv FROM MasterVendorTvCable tv WHERE tv.id = :id AND tv.active = true")
    List<MasterVendorTvCable> findById(Long id);

    @Query("SELECT tv FROM MasterVendorTvCable tv WHERE tv.id = :id AND tv.active = true")
    MasterVendorTvCable findAById(Long id);

    @Query("SELECT tv FROM MasterVendorTvCable tv WHERE tv.active = true")
    List<MasterVendorTvCable> findAllActive();
}
