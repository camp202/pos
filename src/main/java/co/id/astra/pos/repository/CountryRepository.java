package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<MasterCountry, String> {
    MasterCountry findById (Long id);
    @Query("select c from MasterCountry c where c.active = true")
    List<MasterCountry> findAllDataActive();
    MasterCountry findByName (String name);
    List<MasterCountry> getById(Long id);
}
