package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterBankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MasterBankAccountRepository extends JpaRepository <MasterBankAccount, String> {
    MasterBankAccount findById(Long id);
    @Query("select ba from MasterBankAccount  ba where ba.active =true")
    public List<MasterBankAccount> findAllDataActive();

    public List<MasterBankAccount> getById(Long id);
}
