package co.id.astra.pos.repository;

//import co.id.astra.pos.model.entity.master.MasterCustomer;
import co.id.astra.pos.model.entity.master.MasterCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<MasterCustomer, String> {

    MasterCustomer findById(Long id);

    @Query("select u from MasterCustomer u where u.active = true")
    List<MasterCustomer> findAllDataActive();
}
