package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterProvince;
import co.id.astra.pos.model.entity.master.MasterRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegionRepository extends JpaRepository<MasterRegion, String> {


    List<MasterRegion> findAllByMasterProvince(MasterProvince masterProvince);

    List<MasterRegion> findAllById(Long id);

    MasterRegion findById(Long id);

    MasterRegion findByName(String name);

    @Query("select r from MasterRegion r where r.active = true")
    List<MasterRegion> findAllDataActive();
}
