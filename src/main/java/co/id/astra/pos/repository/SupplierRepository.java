package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterSupplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SupplierRepository extends JpaRepository <MasterSupplier, String> {
    List<MasterSupplier> findById(Long id);

    @Query("select s from MasterSupplier s where s.active=true")
    List<MasterSupplier> findAllDataActive();
    @Query("select m from MasterSupplier m where m.id= :id and m.active=true")
    MasterSupplier getById(Long id);

}
