package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<MasterCategory, String> {

    MasterCategory findById (Long id);

    @Query("SELECT c from MasterCategory c where c.id= :id")
    List<MasterCategory> getById(Long id);

    @Query("select mc from MasterCategory mc where mc.active = true")
    List<MasterCategory> findAllDataActive();



//    @Query("SELECT mc from MasterCategory mc LEFT JOIN MasterItem mi ON mc.id = mi.category_id")

}
