package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterVendorInternet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorInternetRepository extends JpaRepository<MasterVendorInternet, String> {

    @Query("SELECT o FROM MasterVendorInternet o WHERE o.id = :id")
    MasterVendorInternet getById(Long id);

   MasterVendorInternet findById(Long Id);

    @Query("select u from MasterVendorInternet u where u.active = true" )
    List<MasterVendorInternet> findAllDataActive();

    List<MasterVendorInternet> findAllById(Long id);
}
