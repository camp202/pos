package co.id.astra.pos.repository;

import co.id.astra.pos.model.entity.master.MasterCardType;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CardTypeRepository extends JpaRepository <MasterCardType, String> {

    MasterCardType findById(Long id);

    @Query("select u from MasterCardType u where u.active = true" )
    List<MasterCardType> findAllDataActive();
}
