package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class OutletGetDto {
    private Long id;

    private String name;

    private String address;

    private String phone;

    private String email;

    private String postalCode;

    //    private DistrictGetDto district;
    private RegionGetDto region;
    private ProvinceGetDto province;

}

