package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class ItemInvGetDto {

    private Long id;

//    private Long outletId;

    private Integer beginning;

    private Integer purchaseQty;

    private Integer salesOrderQty;

    private Integer transferStockQty;

    private Integer adjustmentQty;

    private Integer endingQty;

    private Integer alertAtQty;
}
