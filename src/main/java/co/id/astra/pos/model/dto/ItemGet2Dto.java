package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemGet2Dto {
    @NotNull
    private Long id;

    private String name;
}
