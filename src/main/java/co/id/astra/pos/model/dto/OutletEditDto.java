package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterDistrict;
import co.id.astra.pos.model.entity.master.MasterProvince;
import co.id.astra.pos.model.entity.master.MasterRegion;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
//dto untuk kontrak data(yang akan dibaca oleh frontend dan back end), data apa yang mau ditampilin

public class OutletEditDto {

    private Long id;

//    private Long provinceId;
//
//    private Long regionId;
//
//    private Long districtId;
    @NotNull
    private String email;
    @NotNull
    private String phone;
    @NotNull
    private String postalCode;
    @NotNull
    private Long modifiedBy;

    private MasterProvince masterProvince;
    private MasterRegion masterRegion;
    //private MasterDistrict masterDistrict;
}
