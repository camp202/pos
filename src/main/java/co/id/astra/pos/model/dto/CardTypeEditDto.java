package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CardTypeEditDto {

    private Long id;
    private String name;
    @NotNull
    private Long modifiedBy;

   }