package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name= MasterVendorTvCable.TABLE_NAME)
public class MasterVendorTvCable extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_tvcable";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name="id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "code", length = 10, nullable = false)
    private String code;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "date_start", nullable = true)
    private Date dateStart;

    @Column(name = "date_end", nullable = true)
    private Date dateEnd;

    @Column(name = "notes", length = 1000, nullable = true)
    private String notes;
}
