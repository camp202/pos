package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class ItemGetDto {

    private Long id;

    private String name;

    private CategoryGetDto category;
}
