package co.id.astra.pos.model.dto;
import lombok.Data;

@Data
public class UserDto {
//    @NotNull
//    private Long id;

    private String username;

    private String password;

    private UserRoleDto masterRole;

    private UserEmployeeDto masterEmployee;

}
