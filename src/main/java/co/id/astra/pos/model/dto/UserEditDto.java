package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterRole;
import lombok.Data;

import javax.validation.constraints.NotNull;

//.model.entity.MasterEmployee;

@Data
public class UserEditDto {
    @NotNull
    private Long id;

    private String username;

    private String password;

    private UserRoleEditDto masterRole;

    private UserEmployeeEditDto masterEmployee;
}
