package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;


import javax.persistence.*;

@Data
@Entity
@Table(name= MasterSupplier.TABLE_NAME)
public class MasterSupplier extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_supplier";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "phone", length = 16)
    private String phone;

    @Column(name = "email", length = 50)
    private String email;

    //many to one ke province
    @ManyToOne
    @JoinColumn(name = "province_id")
    private MasterProvince province;

    //many to one ke region
    @ManyToOne
    @JoinColumn(name = "region_id")
    private MasterRegion region;

    //many to one ke district
    @ManyToOne
    @JoinColumn(name = "district_id")
    private MasterDistrict district;

    @Column(name = "postal_code", length = 6)
    private String postalCode;

}
