package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterCategory;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemEditDto {

    @NotNull
    private Long id;

    private String name;

    private Long categoryId;
    
    @NotNull
    private Long modifiedBy;
}

