package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MasterCategoryDto {
    @NotNull
    private String name;
    @NotNull
    private Long id;
//    @NotNull
//    private Long createdBy;

}
