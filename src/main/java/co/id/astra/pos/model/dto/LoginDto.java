package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String username;
    private String password;
}
