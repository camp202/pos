package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterRegion;
import co.id.astra.pos.model.dto.ProvinceDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class  OutletDto {

    private Long id;
    private String name;

//    //private MasterDistrict districtId;
//
//    private MasterRegion regionId;

    private ProvinceDto provinceId;

}

