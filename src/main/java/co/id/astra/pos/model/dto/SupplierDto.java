package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterDistrict;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SupplierDto {
    @NotNull
    private String name;
    @NotNull
    private Long id;

    private String address;
    private String phone;
    private String email;
    private ProvinceGetDto province = new ProvinceGetDto();
    private RegionGetDto region = new RegionGetDto();
    private DistrictDto district = new DistrictDto();
    private String postalCode;
}
