package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CountryDto {
    @NotNull
    private Long id;
    @NotNull
    private String code;
    @NotNull
    private String name;
//    @NotNull
//    private Long createdBy;
}
