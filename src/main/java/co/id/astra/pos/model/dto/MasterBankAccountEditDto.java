package co.id.astra.pos.model.dto;


import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MasterBankAccountEditDto {
    @NotNull
    private Long id;
    @NotNull
    private String accountName;
    @NotNull
    private String accountNumber;
    @NotNull
    private Long modifiedBy;

    private MasterBank masterBank;
    private MasterOutlet masterOutlet;
}
