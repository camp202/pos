package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemDataListDto {

    private Long idItemVar; // id itemVar

    private Long idItemInv; // id itemInv
    @NotNull
    private String nameItemVar; // name itemVar
    @NotNull
    private Double priceItemVar; // price itemVar
    @NotNull
    private String skuItemVar; // sku itemVar
    @NotNull
    private Integer beginningItemInv; // beginning itemInv
    @NotNull
    private Integer endingQtyItemInv; // beginning itemInv
    @NotNull
    private Integer alertAtQtyItemInv; // alertAtQty itemInv
}