package co.id.astra.pos.model.dto;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryEditDto {
    @NotNull
    private String name;
    @NotNull
    private Long id;
    @NotNull
    private Long modifiedBy;
}
