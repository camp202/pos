package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OutletDeleteDto {

    private Long id;
    @NotNull
    private String email;
    @NotNull
    private Long modifiedBy;
}

