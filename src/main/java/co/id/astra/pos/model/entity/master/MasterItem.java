package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name= MasterItem.TABLE_NAME)
public class MasterItem extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_item";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    //many to one ke category
    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private MasterCategory masterCategory;

    @OneToMany(mappedBy = "masterItem", cascade = CascadeType.ALL)
    private List<MasterItemVariant> itemVariants;

}
