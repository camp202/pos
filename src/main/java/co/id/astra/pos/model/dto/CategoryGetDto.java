package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class CategoryGetDto {

    private Long id;

    private String name;

}
