package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

//.model.entity.MasterEmployee;

@Data
public class UserDeleteDto {
    @NotNull
    private Long id;

}
