package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemSaveDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;

    private Long categoryId;

}
