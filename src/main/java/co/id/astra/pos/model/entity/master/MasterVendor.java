package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name= MasterVendor.TABLE_NAME)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class MasterVendor extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_vendor";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name="code", length = 10, nullable = false)
    private String code;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "address", length = 255)
    private String address;

    @Column(name = "phone", length = 16)
    private String phone;

    @Column(name = "email", length = 50)
    private String email;

    @ManyToOne
    @JoinColumn(name = "province_id", referencedColumnName = "id")
    private MasterProvince masterProvince;

    @ManyToOne
    @JoinColumn(name = "region_id", referencedColumnName = "id")
    private MasterRegion masterRegion;

    @ManyToOne
    @JoinColumn(name = "district_id", referencedColumnName = "id")
    private MasterDistrict masterDistrict;

    @Column(name = "postal_code", length = 6)
    private String postalCode;
}
