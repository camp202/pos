package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterRegion;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class DistrictGetDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;
    @NotNull
    private RegionGetDto region;
}
