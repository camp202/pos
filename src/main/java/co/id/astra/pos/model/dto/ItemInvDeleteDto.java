package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemInvDeleteDto {
    @NotNull
    private Long id;

    private Long modifiedBy;
}
