package co.id.astra.pos.model.entity;

import co.id.astra.pos.model.entity.CommonEntity;
import co.id.astra.pos.model.entity.master.MasterItem;
import co.id.astra.pos.model.entity.master.MasterItemVariant;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= ItemInventory.TABLE_NAME)
public class ItemInventory extends CommonEntity {
    public static final String TABLE_NAME = "pos_item_inventory";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    //many to one ke item variant
    @ManyToOne
    @JoinColumn(name = "variant_id", referencedColumnName = "id")
        private MasterItemVariant masterItemVariant;

    //many to one ke outlet
    @ManyToOne
    @JoinColumn(name = "outlet_id", referencedColumnName = "id", nullable = false   )
    private MasterOutlet masterOutlet;

    @Column(name = "beginning", nullable = false)
    private Integer beginning;

    @Column(name = "purchase_qty")
    private Integer purchaseQty;

    @Column(name = "sales_order_qty")
    private Integer salesOrderQty;

    @Column(name = "transfer_stock_qty")
    private Integer transferStockQty;

    @Column(name = "adjustment_qty")
    private Integer adjustmentQty;

    @Column(name = "ending_qty", nullable = false)
    private Integer endingQty;

    @Column(name = "alert_at_qty", nullable = false)
    private Integer alertAtQty;

}
