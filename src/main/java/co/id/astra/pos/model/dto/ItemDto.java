package co.id.astra.pos.model.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ItemDto {

    private Long id; // id item

    private String name; // nama item

    private Long categoryId; // categoryId item

    private List<ItemDataListDto> itemList = new ArrayList<>();
}