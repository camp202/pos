package co.id.astra.pos.model.dto;
import co.id.astra.pos.model.entity.master.MasterDistrict;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ChooseOutletDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
}
