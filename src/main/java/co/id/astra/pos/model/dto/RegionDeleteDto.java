package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegionDeleteDto {
    @NotNull
    private Long id;
}
