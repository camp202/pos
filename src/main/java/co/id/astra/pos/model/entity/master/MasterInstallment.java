package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= MasterInstallment.TABLE_NAME)
public class MasterInstallment extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_installment";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "installment_value", length = 50, nullable = false)
    private String installmentValue;
}
