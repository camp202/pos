package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MasterPaymentMethodDto {
    @NotNull
    private String id;
    @NotNull
    private String code;
    @NotNull
    private String name;
    @NotNull
    private String notes;
    private Long createdBy;
}
