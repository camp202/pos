package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name= MasterCategory.TABLE_NAME)
public class MasterCategory extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_category";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;


    //many to one ke category
//    @OneToMany(mappedBy= "masterCategory", cascade = CascadeType.ALL)
//    private List<MasterItem> items = new ArrayList<>();

}
