package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MasterInstallmentDto {
    @NotNull
    private String installmentValue;
    @NotNull
    private Long id;

    private Long createdBy;
}
