package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

//.model.entity.MasterEmployee;

@Data
public class EmployeeEditDto {
    @NotNull
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String title;

    private String haveAccount;

//    private List<OutletDto> masterOutlets;
}
