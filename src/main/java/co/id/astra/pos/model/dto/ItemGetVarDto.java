package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class ItemGetVarDto {
    @NotNull
    private Long id;

    private String name;

    private CategoryGetDto category;

    private List<ItemVarGetInvDto> itemVariants = new ArrayList<>();
}
