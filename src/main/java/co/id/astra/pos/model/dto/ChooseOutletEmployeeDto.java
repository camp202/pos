package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

@Data
public class ChooseOutletEmployeeDto {
    @NotNull
    private Long id;
    @NotNull
    private String firstName;
    @NotNull
    private Collection<ChooseOutletDto> masterOutlets;
}
