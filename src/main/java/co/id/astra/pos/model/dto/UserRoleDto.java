package co.id.astra.pos.model.dto;
import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterRole;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserRoleDto {
    @NotNull
    private Long id;
    private String name;

}
