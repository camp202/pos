package co.id.astra.pos.model.entity.master;

import com.fasterxml.jackson.annotation.JsonFormat;
import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name= MasterCustomer.TABLE_NAME)
public class MasterCustomer extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_customer";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "email", length = 50, nullable = false)
    private String email;

    @Column(name = "phone", length = 16)
    private String phone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dob")
    private Date dob;

    @Column(name = "address")
    private String address;

    //many to one ke province
    @ManyToOne
    @JoinColumn(name = "province_id")
    private MasterProvince masterProvince;

    //many to one ke region
    @ManyToOne
    @JoinColumn(name = "region_id")
    private MasterRegion masterRegion;

    //many to one ke district
    @ManyToOne
    @JoinColumn(name = "district_id")
    private MasterDistrict masterDistrict;

}
