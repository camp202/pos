package co.id.astra.pos.model.dto.district;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DistrictModifiedDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private RegionModifiedDto region;
}
