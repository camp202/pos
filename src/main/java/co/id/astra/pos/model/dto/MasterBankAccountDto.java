package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MasterBankAccountDto {
    @NotNull
    private String id;
    private BankDto masterBank =new BankDto();

    private OutletDto masterOutlet = new OutletDto();

    @NotNull
    private String accountName;
    @NotNull
    private String accountNumber;
}
