package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= MasterUser.TABLE_NAME)
public class MasterUser extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_user";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "username", length = 50, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    //many to one ke role
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private MasterRole masterRole;

    //many to one ke employee
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private MasterEmployee masterEmployee;

    @Column(name = "is_locked", nullable = false)
    private Boolean isLocked = false;

}
