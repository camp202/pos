package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class LoginShowDto {
    private String username;
    private String password;
    private LoginRoleDto role;
    private LoginEmployeeDto employee;
}
