package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= MasterBarcode.TABLE_NAME)
public class MasterBarcode extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_barcode";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "variant_id", nullable = false)
    private Long variantId;

    @Column(name = "manual_prefix", length = 5)
    private String manualPrefix;

    @Column(name = "size", length = 10, nullable = false)
    private String size;

    @Column(name = "barcode_prefix", length = 5)
    private String barcodePrefix;

    @Column(name = "barcode", length = 15, nullable = false)
    private String barcode;
}
