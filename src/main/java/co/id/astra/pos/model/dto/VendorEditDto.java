package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class VendorEditDto {

    private Long id;
    @NotNull
    private String email;
    @NotNull
    private String phone;
    @NotNull
    private String postalCode;

    private Long modifiedBy;

    //private MasterDistrict districtId;

    private RegionDto regionId = new RegionDto();

    private ProvinceDto provinceId = new ProvinceDto() ;
}
