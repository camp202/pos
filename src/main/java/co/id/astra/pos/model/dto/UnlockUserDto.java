package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UnlockUserDto {
    private MasterEmployee masterEmployee;

    @NotNull
    private Long id;
    @NotNull
    private Boolean isLocked = false;
}
