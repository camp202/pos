package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class CardTypeDto {

    private String name;

    private Long id;

}
