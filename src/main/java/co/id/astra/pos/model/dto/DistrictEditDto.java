package co.id.astra.pos.model.dto;

import javax.validation.constraints.NotNull;

public class DistrictEditDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
}
