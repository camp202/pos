package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class InstallmentGetDto {
    @NotNull
    private String installmentValue;
    @NotNull
    private Long id;

}

