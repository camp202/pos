package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class ChangePassDto {
    private Long id;
    private String username;
    private String password;
}
