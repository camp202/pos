package co.id.astra.pos.model.dto.tvcable;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class VendorTvCableEditDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String code;
    @NotNull
    private String notes;
}
