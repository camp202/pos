package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class ItemVarGetInvDto {
    @NotNull
    private Long id;

    private String name;

    private String sku;

    private Double price;

    private List<ItemInvGetDto> itemInventories = new ArrayList<>();
}
