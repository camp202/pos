package co.id.astra.pos.model.dto;
import co.id.astra.pos.model.entity.master.MasterUser;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
public class EmployeeDto {
//    @NotNull
//    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String title;

    private String haveAccount;

    private List<OutletDto> masterOutlets;

//    private List<MasterUser> masterUser;
//    private MasterUser masterUserSet;
}
