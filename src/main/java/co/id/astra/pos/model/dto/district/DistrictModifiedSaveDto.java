package co.id.astra.pos.model.dto.district;

import co.id.astra.pos.model.entity.master.MasterRegion;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DistrictModifiedSaveDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;

    private RegionModifiedSaveDto masterRegion;
}
