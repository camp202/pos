package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import co.id.astra.pos.model.entity.ItemInventory;
import com.sun.naming.internal.FactoryEnumeration;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name= MasterItemVariant.TABLE_NAME)
public class MasterItemVariant extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_item_variant";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    //many to one ke item
    @ManyToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private MasterItem masterItem;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sku", length = 50, nullable = false)
    private String sku;

    @Column(name = "price", nullable = false)
    private Double price;

    @OneToMany(mappedBy = "masterItemVariant", cascade = CascadeType.ALL)
    private List<ItemInventory> itemInventory;
}
