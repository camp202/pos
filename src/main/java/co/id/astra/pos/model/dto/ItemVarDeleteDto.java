package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemVarDeleteDto {
    @NotNull
    private Long id;

    private Long modifiedBy;
}
