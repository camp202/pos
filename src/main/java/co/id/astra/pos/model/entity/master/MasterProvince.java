package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name= MasterProvince.TABLE_NAME)
public class MasterProvince extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_province";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="id")
    private List<MasterRegion> regions;
}
