package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemInvEditDto {
    @NotNull
    private Long id;
    @NotNull
    private Integer alertAtQty;

    private Long modifiedBy;
}
