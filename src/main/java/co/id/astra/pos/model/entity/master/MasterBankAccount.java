package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= MasterBankAccount.TABLE_NAME)
public class MasterBankAccount extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_bank_account";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    //many to one ke bank
    @ManyToOne
    @JoinColumn(name = "bank_id", referencedColumnName = "id")
    private MasterBank masterBank;

    //many to one ke outlet
    @ManyToOne
    @JoinColumn(name = "outlet_id")
    private MasterOutlet masterOutlet;

    @Column(name = "account_number", length = 50, nullable = false, unique = true)
    private String accountNumber;

    @Column(name = "account_name", length = 100, nullable = false, unique = true)
    private String accountName;

}
