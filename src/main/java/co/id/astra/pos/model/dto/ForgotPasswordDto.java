package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ForgotPasswordDto {
    private MasterEmployee masterEmployee;

    @NotNull
    private Long id;
    @NotNull
    private String email;
    @NotNull
    private String password;
}
