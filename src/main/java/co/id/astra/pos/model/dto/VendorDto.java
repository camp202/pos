package co.id.astra.pos.model.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class VendorDto {

    private Long id;
    private String code;
    private String name;
    @NotNull
    private String address;
    @NotNull
    private String phone;
    @NotNull
    private String email;
    @NotNull
    private String postalCode;

    //private MasterDistrict districtId;

    private RegionDto regionId = new RegionDto();

    private ProvinceDto provinceId = new ProvinceDto() ;

}
