package co.id.astra.pos.model.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class RoleDto {
    @NotNull
    private Long id;
    private String code;
    private String name;
}
