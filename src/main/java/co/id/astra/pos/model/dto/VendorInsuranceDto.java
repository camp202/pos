package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class VendorInsuranceDto {
    @NotNull
    private Long id;

    private String code;

    private String name;

    private String notes;
}
