package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemVarEditDto {

    @NotNull
    private Long id;
    @NotNull
    private Double price;

    private Long modifiedBy;
}
