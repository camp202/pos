package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class LoginEmployeeDto {
    private Long id;
    private String firstName;
    private String lastName;
    private Boolean haveAccount;
}
