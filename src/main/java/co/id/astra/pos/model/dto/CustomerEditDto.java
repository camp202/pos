package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class CustomerEditDto {

    @NotNull
    private Long id;

    private String name;

    @NotNull
    private String phone;

    private String email;

    private Date dob;

    private ProvinceDto Province;

    private RegionDto Region;

    private DistrictDto District;
}
