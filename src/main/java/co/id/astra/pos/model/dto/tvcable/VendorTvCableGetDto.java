package co.id.astra.pos.model.dto.tvcable;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class VendorTvCableGetDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String code;

    private String notes;

    private Date dateStart;

    private Date dateEnd;
}
