package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterProvince;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegionEditDto {
    @NotNull
    private Long id;

    private ProvinceSaveDto masterProvince;
    @NotNull
    private String name;
}
