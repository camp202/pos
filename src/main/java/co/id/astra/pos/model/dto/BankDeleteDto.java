package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterCountry;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class BankDeleteDto {
    @NotNull
    private Long id;
    @NotNull
    private Long modifiedBy;
}
