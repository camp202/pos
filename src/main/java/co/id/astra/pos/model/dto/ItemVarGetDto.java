package co.id.astra.pos.model.dto;

import lombok.Data;

@Data
public class ItemVarGetDto {

    private Long id;

    private ItemGetDto item = new ItemGetDto();

    private String name;

    private String sku;

    private Double price;
}
