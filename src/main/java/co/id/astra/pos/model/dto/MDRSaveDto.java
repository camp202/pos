package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class MDRSaveDto {

    private MDREDCDto EDC;

    private String mdrType;

    private Double mdrValue;

    private Date periodTo;

}
