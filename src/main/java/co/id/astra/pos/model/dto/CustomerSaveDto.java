package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class CustomerSaveDto {

    private String name;

    @NotNull
    private String phone;

    private String email;

    private Date dob;

    private String address;

    private ProvinceDto Province;

    private RegionDto Region;

    private DistrictDto District;
}
