package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CountryDeleteDto {
    @NotNull
    private Long id;
    @NotNull
    private Long modifiedBy;
}
