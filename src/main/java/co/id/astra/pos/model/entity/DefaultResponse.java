package co.id.astra.pos.model.entity;

import lombok.Data;

@Data //lombok plugins otomatis setter getter tapi klo di intelliJ harus ada konfigurasi tersendiri
public class DefaultResponse<T> {
    private String message;
    private String status;
    private T data;

    public DefaultResponse(Boolean isSuccess) {
        this.message = "success";
        this.status = "00";
    }
}
