package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterCountry;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class BankDto {
    @NotNull
    private Long id;
    @NotNull
    private String code;
    @NotNull
    private String name;
//    @NotNull
//    private Long countryId;

    private CountryDto country = new CountryDto();
}
