package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterDistrict;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SupplierEditDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    private String address;
    private String phone;
    private String email;
    private ProvinceEditDto province;
    private RegionEditDto region;
    private MasterDistrict district;
}
