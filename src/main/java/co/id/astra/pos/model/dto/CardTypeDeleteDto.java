package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CardTypeDeleteDto {

    private Long id;

    private String nama;
    @NotNull
    private Long modifiedBy;
}
