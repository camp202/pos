package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= MasterBank.TABLE_NAME)
public class MasterBank extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_bank";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    //many to one ke country
    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    private MasterCountry masterCountry;

    @Column(name = "code", nullable = false, length = 10, unique = true)
    private String code;

    @Column(name = "name", length = 100, nullable = false, unique = true)
    private String name;

}
