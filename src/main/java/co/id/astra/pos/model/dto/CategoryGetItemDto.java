package co.id.astra.pos.model.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryGetItemDto {

    private Long id;
    private String name;
    private List<ItemGet2Dto> items = new ArrayList<>();

}
