package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CustomerDto {
    @NotNull
    private Long id;

    private String name;

    @NotNull
    private String phone;

    private String email;


}
