package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EDCEditDto {

    @NotNull
    private Long id;

    private EDCOutletDto Outlet;

    private EDCBankDto Bank;

    private EDCCardTypeDto CardType;

    private EDCInstallmentDto Installment;

    private String midNumber;
}
