package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProvinceGetRegionDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;

    private List<RegionDto> regions = new ArrayList<>();
}
