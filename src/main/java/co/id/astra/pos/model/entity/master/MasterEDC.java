package co.id.astra.pos.model.entity.master;

import co.id.astra.pos.model.entity.CommonEntity;
import lombok.Data;
import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.model.entity.master.MasterOutlet;

import javax.persistence.*;

@Data
@Entity
@Table(name= MasterEDC.TABLE_NAME)
public class MasterEDC extends CommonEntity {
    public static final String TABLE_NAME = "pos_mst_edc";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "bank_id")
    private MasterBank masterBank;

    @ManyToOne
    @JoinColumn(name = "outlet_id")
    private MasterOutlet masterOutlet;

    @ManyToOne
    @JoinColumn(name = "card_type_id")
    private MasterCardType masterCardType;

    @ManyToOne
    @JoinColumn(name = "installment_id")
    private MasterInstallment masterInstallment;

    @Column(name = "mid_number", length = 30)
    private String midNumber;
}