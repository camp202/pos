package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemVarSaveDto {
    @NotNull
    private Long id;
    @NotNull
    private Long item;
    @NotNull
    private String name;
    @NotNull
    private String sku;
    @NotNull
    private Double price;
}
