package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ItemInvSaveDto {
    @NotNull
    private Long id;

    private Long variantId;

    private Long outletId;
    @NotNull
    private Integer beginning;
    @NotNull
    private Integer alertAtQty;
}
