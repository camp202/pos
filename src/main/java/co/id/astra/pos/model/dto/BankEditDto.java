package co.id.astra.pos.model.dto;

import co.id.astra.pos.model.entity.master.MasterCountry;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BankEditDto {
    @NotNull
    private Long id;
    @NotNull
    private Long countryId;
    @NotNull
    private String code;
    @NotNull
    private String name;

    private MasterCountry country;
//    @NotNull
//    private Long modifiedBy;
}
