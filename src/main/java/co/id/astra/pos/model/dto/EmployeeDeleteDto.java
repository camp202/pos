package co.id.astra.pos.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

//.model.entity.MasterEmployee;

@Data
public class EmployeeDeleteDto {
    @NotNull
    private Long id;

}
