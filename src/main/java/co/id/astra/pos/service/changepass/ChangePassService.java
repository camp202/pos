package co.id.astra.pos.service.changepass;

import co.id.astra.pos.model.entity.master.MasterUser;

public interface ChangePassService {
    MasterUser edit(MasterUser passEdit);
}
