package co.id.astra.pos.service.paymentmethod;

import co.id.astra.pos.model.entity.master.MasterPaymentMethod;
import co.id.astra.pos.repository.MasterPaymentMethodRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

@Service
@Transactional
public class MasterPaymentMethodServiceImpl implements MasterPaymentMethodService{
    @Autowired
    private MasterPaymentMethodRepository masterPaymentMethodRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterPaymentMethod> findById(Long id){ return masterPaymentMethodRepository.findById(id); }

    @Override
    public MasterPaymentMethod save(MasterPaymentMethod masterPaymentMethod){
        Date currentDate = new Date();
        masterPaymentMethod.setCreatedOn(currentDate);
        masterPaymentMethod.setActive(Boolean.TRUE);
        return masterPaymentMethodRepository.save(masterPaymentMethod);
    }

    @Override
    public MasterPaymentMethod delete(MasterPaymentMethod masterPaymentMethodDelete){
        Date currentDate = new Date();
        MasterPaymentMethod masterPaymentMethod = masterPaymentMethodRepository.getById(masterPaymentMethodDelete.getId());
        masterPaymentMethod.setModifiedOn(currentDate);
        masterPaymentMethod.setActive(Boolean.FALSE);
        masterPaymentMethod.setModifiedBy(masterPaymentMethodDelete.getModifiedBy());
        masterPaymentMethodRepository.save(masterPaymentMethod);
        return masterPaymentMethod;
    }

    @Override
    public MasterPaymentMethod update(MasterPaymentMethod masterPaymentMethod){
        Date currentDate = new Date();
        return null;
    }

    @Override
    public List<MasterPaymentMethod> findAll() {
        return masterPaymentMethodRepository.findAllDataActive();

    }

    @Override
    public MasterPaymentMethod edit(MasterPaymentMethod masterPaymentMethodEdit){
        Date currentDate = new Date();
        MasterPaymentMethod masterPaymentMethod = masterPaymentMethodRepository.getById(masterPaymentMethodEdit.getId());
        masterPaymentMethod.setModifiedOn(currentDate);
        modelMapper.map(masterPaymentMethodEdit, masterPaymentMethod);
        masterPaymentMethodRepository.save(masterPaymentMethod);
        return masterPaymentMethod;
    }

}
