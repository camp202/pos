package co.id.astra.pos.service.Item;

import co.id.astra.pos.model.dto.ItemDataListDto;
import co.id.astra.pos.model.dto.ItemDto;
import co.id.astra.pos.model.dto.ItemVarGetInvDto;
import co.id.astra.pos.model.entity.ItemInventory;
import co.id.astra.pos.model.entity.master.MasterItem;
import co.id.astra.pos.model.entity.master.MasterItemVariant;

import java.util.List;

public interface ItemService {


    List<MasterItem> findAllItem();

    ItemDto save(ItemDto items);

    ItemDto edit(ItemDto items);

    ItemDto deleteItem(ItemDto items);

    ItemDataListDto deleteItem(ItemDataListDto itemData);
//
//    MasterItem deleteItem(MasterItem itemDelete);
//
//    MasterItemVariant deleteItemVar(MasterItemVariant itemVarDelete);
//
//    ItemInventory deleteItemInv(MasterItemVariant itemVarDelete);
}
