package co.id.astra.pos.service.role;

import co.id.astra.pos.model.entity.master.MasterRole;
import co.id.astra.pos.repository.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ModelMapper modelMapper;


    @Override
    public List<MasterRole> findById(Long id) {
        return roleRepository.findById(id);
    }

    @Override
    public MasterRole getById(Long id){
        return roleRepository.getById(id);
    }

    @Override
    public MasterRole save(MasterRole role) {
        Date currentDate = new Date();
        role.setActive(Boolean.TRUE);
        role.setCreatedOn(currentDate);
        return roleRepository.save(role);
    }

    @Override
    public List<MasterRole> findAll() {
        return roleRepository.findAllDataActive();
    }


    @Override
    public MasterRole edit(MasterRole roleEdit) {
        Date currentDate = new Date();
        MasterRole role = roleRepository.getById(roleEdit.getId());
        role.setModifiedOn(currentDate);
        modelMapper.map(roleEdit, role);
        roleRepository.save(role);
        return role;
    }

    @Override
    public MasterRole update(MasterRole role){
        Date currentDate = new Date();
        return null;
    }
}


