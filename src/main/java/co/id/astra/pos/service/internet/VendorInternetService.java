package co.id.astra.pos.service.internet;

import co.id.astra.pos.model.entity.master.MasterVendorInternet;

import java.util.List;

public interface VendorInternetService {
    List<MasterVendorInternet> findById(Long id);
//    MasterVendorInternet findById(Long id);

    MasterVendorInternet save(MasterVendorInternet masterVendorInternet);

    MasterVendorInternet delete(MasterVendorInternet masterDelete);
        List<MasterVendorInternet> findAll();
//    MasterVendorInternet update(MasterVendorTelecommunication masterVendorTelecommunication);

    MasterVendorInternet deleteId(Long idLeasing);
    MasterVendorInternet edit(MasterVendorInternet masterEdit);

}
