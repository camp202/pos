package co.id.astra.pos.service.outlet;

import co.id.astra.pos.model.entity.master.MasterOutlet;

import java.util.List;
import java.util.Optional;

public interface OutletService {

    MasterOutlet findById(Long id);

    MasterOutlet save(MasterOutlet masterOutlet);

    MasterOutlet delete(MasterOutlet masterDelete);

    MasterOutlet update(MasterOutlet masterOutlet);

     List<MasterOutlet> findAll();

    MasterOutlet edit(MasterOutlet masterEdit);
}

