package co.id.astra.pos.service.installment;

import co.id.astra.pos.model.entity.master.MasterInstallment;

import java.util.List;

public interface InstallmentService {
    MasterInstallment findById (Long id);
    MasterInstallment save (MasterInstallment installment);
//    MasterInstallment delete (MasterInstallment installmentDelete);
    MasterInstallment update (MasterInstallment installment);
    List<MasterInstallment> findAll();
    List<MasterInstallment> getById(Long id);
    MasterInstallment edit (MasterInstallment installmentEdit);
}
