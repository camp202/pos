package co.id.astra.pos.service.employee;

import co.id.astra.pos.model.entity.master.MasterOutlet;
import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.repository.EmployeeRepository;
//import co.id.astra.pos.model.entity.master.Privilege;
//import co.id.astra.pos.model.entity.master.Role;
import co.id.astra.pos.model.entity.master.MasterEmployee;
//import co.id.astra.pos.repository.RoleRepository;
import co.id.astra.pos.repository.OutletRepository;
import co.id.astra.pos.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private OutletRepository outletRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterEmployee findById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public MasterEmployee save(MasterEmployee employee) {
        Date currentDate = new Date();
        employee.setCreatedOn(currentDate);
        employee.setActive(Boolean.TRUE);

        return employeeRepository.save(employee);
    }

//    public User saveId(User user) {
//        user.set(generateNomorIDUser(user));
//        return userRepository.save(user);
//    }
//
//    private String generateNomorIDUser(User user){
//        String id = "RM_" + user.getId();
//        return id;
//    }

    @Override
    public MasterEmployee delete(MasterEmployee employeeDelete) {
        Date currentDate = new Date();
        // informasi deletenya jangan lupa diberikan
        // delete itu actualnya cuma ganti status
        MasterEmployee employee = employeeRepository.findById(employeeDelete.getId());
        employeeDelete.setModifiedOn(currentDate);
        employee.setActive(Boolean.FALSE);
        employeeRepository.save(employee);

        return employee;
    }

    @Override
    public MasterEmployee update(MasterEmployee employee) {
        //Jika ada updete keterangan modifiednya jangan lupa di update
        Date currenDate = new Date();
        employee.setModifiedBy(employee.getId());
        employee.setModifiedOn(currenDate);
        return null;
    }

    @Override
    public List<MasterEmployee> findAll() {
//        return employeeRepository.findAllDataActiveEmp();
//        return userRepository.findAllDataActiveEmployee();
        return employeeRepository.findAll();
    }

//    @Override
//    public List<MasterUser> findAllEmployee() {
////        return employeeRepository.findAllDataActiveEmp();
//        return userRepository.findAllDataActiveEmployee();
////        return userRepository.findAll();
//    }

    @Override
    public MasterEmployee edit(MasterEmployee employeeEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya dengan getEmail(); agar nanti yang tidak ada di DTO  tetap data yang current di DB
        MasterEmployee employee = employeeRepository.findById(employeeEdit.getId());
        employee.setModifiedOn(currentDate);
        // mappingkan yang mau di eddit dengan data current yang ada di DB yaitu si user
        // userEdit yang di mapping ke user
        modelMapper.map(employeeEdit, employee);
        //setelah sudah di maping maka yang user di save
        employeeRepository.save(employee);
        return employee;
    }

}
