package co.id.astra.pos.service.supplier;

import co.id.astra.pos.model.entity.master.MasterSupplier;
import co.id.astra.pos.repository.SupplierRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterSupplier> findById(Long id) {
        return supplierRepository.findById(id);
    }

    @Override
    public MasterSupplier getById(Long id){
        return supplierRepository.getById(id);
    }

    @Override
    public MasterSupplier save(MasterSupplier supplier) {
        Date currentDate = new Date();
        supplier.setActive(Boolean.TRUE);
        supplier.setCreatedOn(currentDate);
        return supplierRepository.save(supplier);
    }

    @Override
    public List<MasterSupplier> findAll() {
        return supplierRepository.findAllDataActive();
    }


    @Override
    public MasterSupplier edit(MasterSupplier supplierEdit) {
        Date currentDate = new Date();
        MasterSupplier supplier = supplierRepository.getById(supplierEdit.getId());
        supplier.setModifiedOn(currentDate);
        modelMapper.map(supplierEdit, supplier);
        supplierRepository.save(supplier);
        return supplier;
    }

    @Override
    public MasterSupplier update(MasterSupplier supplier){
        Date currentDate = new Date();
        return null;
    }

    @Override
    public MasterSupplier deleteId(Long id) {
        Date currentDate = new Date();
        MasterSupplier supplier = supplierRepository.getById(id);
        supplier.setModifiedOn(currentDate);
        supplier.setActive(Boolean.FALSE);
        supplierRepository.save(supplier);
        return supplier;
    }
}


