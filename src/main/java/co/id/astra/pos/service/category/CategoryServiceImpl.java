package co.id.astra.pos.service.category;

import co.id.astra.pos.model.entity.master.MasterCategory;
import co.id.astra.pos.repository.CategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterCategory findById(Long id){return categoryRepository.findById(id);}


    @Override
    public MasterCategory save(MasterCategory masterCategory){
        Date currentDate = new Date();
        masterCategory.setCreatedOn(currentDate);
        masterCategory.setCreatedBy(masterCategory.getModifiedBy());
//        masterCategory.setId(generateCategoriId(masterCategory));
        return categoryRepository.save(masterCategory);
    }

    @Override
    public MasterCategory delete(MasterCategory masterCategoryDelete) {
        Date currentDate = new Date();
        MasterCategory masterCategory = categoryRepository.findById(masterCategoryDelete.getId());
        masterCategory.setActive(Boolean.FALSE);
        masterCategory.setModifiedOn(currentDate);
        masterCategory.setModifiedBy(masterCategoryDelete.getModifiedBy());
        categoryRepository.save(masterCategory);

        return masterCategory;
    }

    @Override
    public MasterCategory deleteId (Long id) {
        Date currentDate = new Date();
        MasterCategory category = categoryRepository.findById(id);
        category.setModifiedOn(currentDate);
        category.setActive(Boolean.FALSE);
        categoryRepository.save(category);
        return category;
    }

    @Override
    public MasterCategory update(MasterCategory masterCategory) {
        return null;
    }

    @Override
    public List<MasterCategory> findAll(){return categoryRepository.findAllDataActive();}

    @Override
    public List<MasterCategory> getById(Long id){return categoryRepository.getById(id);}


    @Override
    public MasterCategory edit(MasterCategory masterCategoryEdit) {
        MasterCategory masterCategory = categoryRepository.findById(masterCategoryEdit.getId());
        modelMapper.map(masterCategoryEdit, masterCategory);
        categoryRepository.save(masterCategory);
        return masterCategory;
    }

}
