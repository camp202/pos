package co.id.astra.pos.service.customer;

//import co.id.astra.pos.model.entity.master.MasterCustomer;

import co.id.astra.pos.model.entity.master.MasterCustomer;

import java.util.List;

public interface CustomerService {
    List<MasterCustomer> findAll();
    MasterCustomer save(MasterCustomer customer);
    MasterCustomer findById(Long id);
    MasterCustomer edit(MasterCustomer masterCustomerEdit);
}
