package co.id.astra.pos.service.role;

import co.id.astra.pos.model.entity.master.MasterRole;

import java.util.List;

public interface RoleService {
    MasterRole save(MasterRole supplier);

    MasterRole update(MasterRole supplier);

    List<MasterRole> findAll();

    MasterRole edit(MasterRole supplierEdit);

    List<MasterRole> findById(Long id);

    MasterRole getById(Long Id);
}
