package co.id.astra.pos.service.chooseemployeeoutlet;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterOutlet;

import java.util.Collection;
import java.util.List;

public interface ChooseOutletService {
    List<MasterEmployee> findAll();
    List<MasterEmployee> getById(Long id);
    List<MasterOutlet> goToOutletById(Long id);
}
