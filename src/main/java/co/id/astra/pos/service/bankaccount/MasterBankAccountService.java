package co.id.astra.pos.service.bankaccount;


import co.id.astra.pos.model.entity.master.MasterBankAccount;

import java.util.List;


public interface MasterBankAccountService {
    MasterBankAccount findById(Long id);
    MasterBankAccount save(MasterBankAccount masterBankAccount);
    MasterBankAccount delete(MasterBankAccount masterBankAccountDelete);
    MasterBankAccount update(MasterBankAccount masterBankAccount);
    List<MasterBankAccount>findAll();
    List<MasterBankAccount>getById(Long id);
    MasterBankAccount edit(MasterBankAccount masterBankAccountEdit);
}



