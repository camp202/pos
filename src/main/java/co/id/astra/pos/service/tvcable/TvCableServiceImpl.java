package co.id.astra.pos.service.tvcable;

import co.id.astra.pos.model.entity.master.MasterVendorTvCable;
import co.id.astra.pos.repository.TvCableRepository;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TvCableServiceImpl implements TvCableService {
    @Autowired
    private TvCableRepository tvCableRepository;
    @Autowired
    private ModelMapper modelMapper;

    public List<MasterVendorTvCable> findAll() {
        return tvCableRepository.findAll();
    }

    public List<MasterVendorTvCable> findAllActive() {
        return tvCableRepository.findAllActive();
    }

    public List<MasterVendorTvCable> findById(Long id) {
        return tvCableRepository.findById(id);
    }

    @Override
    public MasterVendorTvCable save(MasterVendorTvCable masterVendorTvCable) {
        Date currDate = new Date();
        masterVendorTvCable.setCreatedOn(currDate);
        masterVendorTvCable.setActive(Boolean.TRUE);
        return tvCableRepository.save(masterVendorTvCable);
    }

    @Override
    public MasterVendorTvCable edit(MasterVendorTvCable vendorEdit){
        Date currentDate = new Date();
        MasterVendorTvCable masterVendorTvCable1 = tvCableRepository.findAById(vendorEdit.getId());
        masterVendorTvCable1.setModifiedOn(currentDate);
        modelMapper.map(vendorEdit, masterVendorTvCable1);
        tvCableRepository.save(masterVendorTvCable1);
        return masterVendorTvCable1;
    }

    @Override
    public MasterVendorTvCable delete(Long id) {
        Date currentDate = new Date();
        MasterVendorTvCable tvcable = tvCableRepository.findAById(id);
        tvcable.setActive(Boolean.FALSE);
        tvcable.setModifiedOn(currentDate);
        return tvCableRepository.save(tvcable);
    }
}
