package co.id.astra.pos.service.edc;

import co.id.astra.pos.model.entity.master.MasterEDC;
import co.id.astra.pos.repository.EDCRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class EDCServiceImpl implements EDCService {
    @Autowired
    private EDCRepository edcRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterEDC> findAll() {
        return edcRepository.findAll();
    }

    @Override
    public MasterEDC findById(Long id) {
        return edcRepository.findById(id);
    }

    @Override
    public MasterEDC save(MasterEDC masterEDC) {
        Date currentDate = new Date();
//        masterCustomer.setCreatedBy();
        masterEDC.setCreatedOn(currentDate);
//        masterCustomer.setModifiedBy(20);
        masterEDC.setModifiedOn(currentDate);
        return edcRepository.save(masterEDC);
    }

    @Override
    public MasterEDC edit(MasterEDC masterEDCEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya
        MasterEDC edc = edcRepository.findById(masterEDCEdit.getId());

        edc.setModifiedOn(currentDate);
        // mappingkan yang mau di eddit
        modelMapper.map(masterEDCEdit, edc);
        edcRepository.save(edc);
        return edc;
    }

}

