package co.id.astra.pos.service.region;


import co.id.astra.pos.model.entity.master.MasterRegion;

import java.util.List;
import java.util.Optional;

public interface RegionService {


    List<MasterRegion> findAllByMasterProvince(Long provinceId);

    List<MasterRegion> findAllById(Long id);

    MasterRegion findById(Long id);

    MasterRegion findByName(String name);

    MasterRegion save(MasterRegion region);

    List<MasterRegion> findAll();

    MasterRegion delete(MasterRegion regionDelete);

    MasterRegion edit(MasterRegion regionEdit);
}
