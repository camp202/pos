package co.id.astra.pos.service.mdr;

import co.id.astra.pos.model.entity.master.MasterMDR;
import co.id.astra.pos.repository.MDRRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MDRServiceImpl implements MDRService {
    @Autowired
    private MDRRepository mdrRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterMDR> findAll() {
        return mdrRepository.findAll();
    }

    @Override
    public MasterMDR findById(Long id) {
        return mdrRepository.findById(id);
    }

    @Override
    public MasterMDR save(MasterMDR masterMDR) {
        Date currentDate = new Date();
//        masterCustomer.setCreatedBy();
        masterMDR.setCreatedOn(currentDate);
//        masterCustomer.setModifiedBy(20);
        masterMDR.setModifiedOn(currentDate);
        return mdrRepository.save(masterMDR);
    }

    @Override
    public MasterMDR edit(MasterMDR masterMDREdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya
        MasterMDR mdr = mdrRepository.findById(masterMDREdit.getId());

        mdr.setModifiedOn(currentDate);
        // mappingkan yang mau di eddit
        modelMapper.map(masterMDREdit, mdr);
        mdrRepository.save(mdr);
        return mdr;
    }

}

