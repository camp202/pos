package co.id.astra.pos.service.chooseemployeeoutlet;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterOutlet;

import co.id.astra.pos.repository.EmployeeRepository;
import co.id.astra.pos.repository.OutletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ChooseOutletServiceImpl implements ChooseOutletService{

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private OutletRepository outletRepository;

    @Override
    public List<MasterEmployee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public List<MasterEmployee> getById(Long id) {
        return employeeRepository.getById(id);
    }
    @Override
    public List<MasterOutlet> goToOutletById(Long id) {
        return outletRepository.getById(id);
    }
}
