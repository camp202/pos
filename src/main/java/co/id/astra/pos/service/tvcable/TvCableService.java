package co.id.astra.pos.service.tvcable;

import co.id.astra.pos.model.entity.master.MasterVendorTvCable;

import java.util.List;

public interface TvCableService {

    List<MasterVendorTvCable> findAll();
    List<MasterVendorTvCable> findById(Long id);
    List<MasterVendorTvCable> findAllActive();
    MasterVendorTvCable edit(MasterVendorTvCable masterVendorTvCable);
    MasterVendorTvCable save(MasterVendorTvCable masterVendorTvCable);
    MasterVendorTvCable delete(Long id);
}
