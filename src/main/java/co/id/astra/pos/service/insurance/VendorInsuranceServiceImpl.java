package co.id.astra.pos.service.insurance;

import co.id.astra.pos.model.entity.master.MasterInsurance;
import co.id.astra.pos.repository.VendorInsuranceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class VendorInsuranceServiceImpl implements VendorInsuranceService{
    @Autowired
    private VendorInsuranceRepository vendorInsuranceRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterInsurance> findById(Long id) {
        return vendorInsuranceRepository.findById(id);
    }

    @Override
    public MasterInsurance save(MasterInsurance masterInsurance) {
        Date currentDate = new Date();
        masterInsurance.setCreatedOn(currentDate);
        masterInsurance.setActive(Boolean.TRUE);
        masterInsurance.setCreatedBy(Long.valueOf("1"));
        return vendorInsuranceRepository.save(masterInsurance);
    }

    @Override
    public MasterInsurance delete(MasterInsurance insuranceDelete) {
        Date currentDate = new Date();
        MasterInsurance masterInsurance = vendorInsuranceRepository.getById(insuranceDelete.getId());
        masterInsurance.setCreatedOn(currentDate);
        masterInsurance.setActive(Boolean.FALSE);
        vendorInsuranceRepository.save(masterInsurance);
        return masterInsurance;
    }

    @Override
    public MasterInsurance update(MasterInsurance masterInsurance) {
        Date currentDate = new Date();
        return null;
    }

    @Override
    public List<MasterInsurance> findAll() {
        return vendorInsuranceRepository.findAllDataActive();
    }

    @Override
    public MasterInsurance edit(MasterInsurance insuranceEdit) {
        Date currentDate = new Date();
        MasterInsurance masterInsurance = vendorInsuranceRepository.getById(insuranceEdit.getId());
        masterInsurance.setModifiedOn(currentDate);
        modelMapper.map(insuranceEdit, masterInsurance);
        vendorInsuranceRepository.save(masterInsurance);
        return masterInsurance;
    }

//    @Override
//    public MasterInsurance getById(Long id) {
//        return null;
//    }


    @Override
    public MasterInsurance deleteId(Long id) {
        Date currentDate = new Date();
        MasterInsurance insurance = vendorInsuranceRepository.getById(id);
        insurance.setModifiedOn(currentDate);
        insurance.setActive(Boolean.FALSE);
        vendorInsuranceRepository.save(insurance);
        return insurance;
    }
}
