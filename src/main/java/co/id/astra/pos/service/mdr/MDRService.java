package co.id.astra.pos.service.mdr;

import co.id.astra.pos.model.entity.master.MasterMDR;

import java.util.List;

public interface MDRService {
    List<MasterMDR> findAll();
    MasterMDR save(MasterMDR edc);
    MasterMDR findById(Long id);
    MasterMDR edit(MasterMDR masterMDREdit);
}
