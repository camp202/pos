package co.id.astra.pos.service.customer;

import co.id.astra.pos.model.entity.master.MasterCustomer;
import co.id.astra.pos.repository.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterCustomer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public MasterCustomer findById(Long id) {
        return customerRepository.findById(id);
    }

    @Override
    public MasterCustomer save(MasterCustomer masterCustomer) {
        Date currentDate = new Date();
//        masterCustomer.setCreatedBy();
        masterCustomer.setCreatedOn(currentDate);
//        masterCustomer.setModifiedBy(20);
        masterCustomer.setModifiedOn(currentDate);
        return customerRepository.save(masterCustomer);
    }

    @Override
    public MasterCustomer edit(MasterCustomer masterCustomerEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya
        MasterCustomer user = customerRepository.findById(masterCustomerEdit.getId());

        user.setModifiedOn(currentDate);
        // mappingkan yang mau di eddit
        modelMapper.map(masterCustomerEdit, user);
        customerRepository.save(user);
        return user;
    }

}
