package co.id.astra.pos.service.employee;

import co.id.astra.pos.model.entity.master.MasterRole;
import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.repository.RoleRepository;
import co.id.astra.pos.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ModelMapper modelMapper;

    public MasterUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MasterUser user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        Collection<MasterRole> masterRoles = new ArrayList<>();
        masterRoles.add(user.getMasterRole());
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                mapRolesToAuthorities(masterRoles));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<MasterRole> masterRoles) {
        return masterRoles.stream()
                .map(masterRole -> new SimpleGrantedAuthority(masterRole.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public MasterUser save(MasterUser user) {
        Date currentDate = new Date();
        user.setCreatedOn(currentDate);
        user.setActive(Boolean.TRUE);
        return userRepository.save(user);
    }

    @Override
    public List<MasterUser> findAllByIdRole(Long roleId) {
        MasterRole masterRole = new MasterRole();
        masterRole.setId(roleId);
//        ===== INI KENAPA GABISAAAAA??????======
//        return userRepository.findAllByIdRole(masterRole);
        return null;
    }

//    public User saveId(User user) {
//        user.set(generateNomorIDUser(user));
//        return userRepository.save(user);
//    }
//
//    private String generateNomorIDUser(User user){
//        String id = "RM_" + user.getId();
//        return id;
//    }

    @Override
    public MasterUser delete(MasterUser userDelete) {
        Date currentDate = new Date();
        // informasi deletenya jangan lupa diberikan
        // delete itu actualnya cuma ganti status
        MasterUser user = userRepository.findById(userDelete.getId());
        userDelete.setModifiedOn(currentDate);
        user.setActive(Boolean.FALSE);
        userRepository.save(user);

        return user;
    }

    @Override
    public MasterUser update(MasterUser user) {
        //Jika ada updete keterangan modifiednya jangan lupa di update
        Date currenDate = new Date();
        user.setModifiedBy(user.getId());
        user.setModifiedOn(currenDate);
        return null;
    }

    @Override
    public List<MasterUser> findAll() {
        return userRepository.findAllDataActive();
//        return employeeRepository.findAll();
    }


    @Override
    public MasterUser edit(MasterUser userEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya dengan getEmail(); agar nanti yang tidak ada di DTO  tetap data yang current di DB
        MasterUser user = userRepository.findById(userEdit.getId());
        user.setModifiedOn(currentDate);
        // mappingkan yang mau di eddit dengan data current yang ada di DB yaitu si user
        // userEdit yang di mapping ke user
        modelMapper.map(userEdit, user);
        //setelah sudah di maping maka yang user di save
        userRepository.save(user);
        return user;
    }

    @Override
    public MasterUser editLock(MasterUser unlockUser) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya
        MasterUser masterUser = userRepository.findById(unlockUser.getId());
        masterUser.setModifiedOn(currentDate);
        // mappingkan yang mau di eddit
        modelMapper.map(unlockUser, masterUser);
        userRepository.save(masterUser);
        return masterUser;
    }

}
