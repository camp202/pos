package co.id.astra.pos.service.bank;

import co.id.astra.pos.model.entity.master.MasterBank;
import co.id.astra.pos.repository.BankRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

@Service
@Transactional
public class BankServiceImpl implements BankService{
    @Autowired
    private BankRepository bankRepository;
//    @Autowired
//    private CountryRepository countryRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterBank findById(Long id) {
        return bankRepository.findById(id);
    }

    @Override
    public MasterBank save(MasterBank masterBank) {
        Date currentDate = new Date();
        masterBank.setCreatedOn(currentDate);
        masterBank.setActive(Boolean.TRUE);
        return bankRepository.save(masterBank);
    }

    @Override
    public MasterBank delete(MasterBank bankDelete) {
        Date currentDate = new Date();
        MasterBank masterBank = bankRepository.findById(bankDelete.getId());
        masterBank.setModifiedOn(currentDate);
        masterBank.setActive(Boolean.FALSE);
        masterBank.setModifiedBy(bankDelete.getModifiedBy());
        bankRepository.save(masterBank);
        return masterBank;
    }

    @Override
    public MasterBank update(MasterBank masterBank) {
        Date currentDate = new Date();
        return null;
    }

    @Override
    public List<MasterBank> findAll() {
        return bankRepository.findAllDataActive();
    }

    @Override
    public MasterBank edit(MasterBank bankEdit) {
        Date currentDate = new Date();
        MasterBank masterBank = bankRepository.findById(bankEdit.getId());
        masterBank.setModifiedOn(currentDate);
        modelMapper.map(bankEdit, masterBank);
        bankRepository.save(masterBank);
        return masterBank;
    }

    @Override
    public List<MasterBank> getById(Long id) {
        return bankRepository.getById(id);
    }
}
