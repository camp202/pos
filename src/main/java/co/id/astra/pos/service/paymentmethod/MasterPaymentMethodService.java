package co.id.astra.pos.service.paymentmethod;

import co.id.astra.pos.model.entity.master.MasterPaymentMethod;

import  java.util.List;

public interface MasterPaymentMethodService {
    List<MasterPaymentMethod> findById(Long id);
    MasterPaymentMethod save(MasterPaymentMethod masterPaymentMethod);
    MasterPaymentMethod delete(MasterPaymentMethod masterPaymentMethodDelete);
    MasterPaymentMethod update(MasterPaymentMethod masterPaymentMethod);
    List<MasterPaymentMethod>findAll();
    //List<MasterPaymentMethod>getById(Long id);
    MasterPaymentMethod edit(MasterPaymentMethod masterPaymentMethodEdit);
}
