package co.id.astra.pos.service.changepass;

import co.id.astra.pos.model.entity.master.MasterUser;
import co.id.astra.pos.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class ChangePassServiceImpl implements ChangePassService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public MasterUser edit(MasterUser changePass) {
        Date currentDate = new Date();
        MasterUser user = userRepository.findById(changePass.getId());
        user.setModifiedOn(currentDate);
        modelMapper.map(changePass, user);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }
}
