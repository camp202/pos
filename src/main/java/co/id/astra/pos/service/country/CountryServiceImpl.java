package co.id.astra.pos.service.country;

import co.id.astra.pos.model.entity.master.MasterCountry;
import co.id.astra.pos.repository.CountryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterCountry findById(Long id) {
        return countryRepository.findById(id);
    }

    @Override
    public List<MasterCountry> findAll() {
        return countryRepository.findAllDataActive();
    }

    @Override
    public List<MasterCountry> getById(Long id) {
        return countryRepository.getById(id);
    }

    @Override
    public MasterCountry save(MasterCountry masterCountry) {
        Date currentDate = new Date();
        masterCountry.setCreatedOn(currentDate);
        masterCountry.setActive(Boolean.TRUE);
        return countryRepository.save(masterCountry);
    }

    @Override
    public MasterCountry findByName(String name) {
        return countryRepository.findByName(name);
    }

    @Override
    public MasterCountry delete(MasterCountry countryDelete) {
        Date currentDate = new Date();
        MasterCountry masterCountry = countryRepository.findById(countryDelete.getId());
        masterCountry.setModifiedOn(currentDate);
        masterCountry.setActive(Boolean.FALSE);
        masterCountry.setModifiedBy(countryDelete.getModifiedBy());
        countryRepository.save(masterCountry);
        return masterCountry;
    }

    @Override
    public MasterCountry deleteById(Long id) {
        Date currentDate = new Date();
        MasterCountry masterCountry = countryRepository.findById(id);
        masterCountry.setModifiedOn(currentDate);
        masterCountry.setActive(Boolean.FALSE);
        countryRepository.save(masterCountry);
        return masterCountry;
    }

    @Override
    public MasterCountry update(MasterCountry masterCountry) {
        Date currentDate = new Date();
        masterCountry.setModifiedOn(currentDate);
        return null;
    }

    @Override
    public MasterCountry edit(MasterCountry countryEdit) {
        Date currentDate = new Date();
        MasterCountry masterCountry = countryRepository.findById(countryEdit.getId());
        masterCountry.setModifiedOn(currentDate);
        modelMapper.map(countryEdit, masterCountry);
        countryRepository.save(masterCountry);
        return masterCountry;
    }

}
