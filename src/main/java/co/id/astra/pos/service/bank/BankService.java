package co.id.astra.pos.service.bank;

import co.id.astra.pos.model.entity.master.MasterBank;

import java.util.List;

public interface BankService {
    MasterBank findById (Long id);
    MasterBank save (MasterBank masterBank);
    MasterBank delete (MasterBank bankDelete);
    MasterBank update (MasterBank masterBank);
    List<MasterBank> findAll();
    MasterBank edit (MasterBank bankEdit);
    List<MasterBank> getById(Long id);
}
