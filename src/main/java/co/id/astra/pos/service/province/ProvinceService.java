package co.id.astra.pos.service.province;

import co.id.astra.pos.model.entity.master.MasterProvince;

import java.util.List;

public interface ProvinceService {

    List<MasterProvince> findById(Long id);

    MasterProvince findByName(String name);

    MasterProvince save(MasterProvince province);

    List<MasterProvince> findAll();

    MasterProvince delete(MasterProvince provinceDelete);

    MasterProvince edit(MasterProvince provinceEdit);
}
