package co.id.astra.pos.service.internet;

import co.id.astra.pos.model.entity.master.MasterVendorInternet;
import co.id.astra.pos.repository.VendorInternetRepository;
import co.id.astra.pos.service.internet.VendorInternetService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class VendorInternetServiceImpl implements VendorInternetService {
    @Autowired
    private VendorInternetRepository vendorInternetRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterVendorInternet> findById(Long id) {
        return vendorInternetRepository.findAllById(id);
    }

    @Override
    public MasterVendorInternet save(MasterVendorInternet masterVendorInternet) {
        Date currentDate = new Date();
        masterVendorInternet.setCreatedOn(currentDate);
        masterVendorInternet.setActive(Boolean.TRUE);
        return vendorInternetRepository.save(masterVendorInternet);
    }

    @Override
    public MasterVendorInternet delete(MasterVendorInternet vendorInternetDelete) {
        Date currentDate = new Date();
        // informasi delete-nya jangan lupa diberikan
        // delete itu actual-nya cuma ganti status
        MasterVendorInternet masterVendorInternet = vendorInternetRepository.findById(vendorInternetDelete.getId());
        masterVendorInternet.setModifiedOn(currentDate);
        masterVendorInternet.setActive(Boolean.FALSE);
        masterVendorInternet.setModifiedBy(vendorInternetDelete.getModifiedBy());
        vendorInternetRepository.save(masterVendorInternet);

        return masterVendorInternet;
    }

    @Override
    public List<MasterVendorInternet> findAll() {
        return vendorInternetRepository.findAllDataActive();
    }

    public MasterVendorInternet edit(MasterVendorInternet vendorInternetEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya agar saat di edit data yang lama tidak null
        // karna yg diedit kan cuma sebagian data, nah kalau data yg lama pasti ter-replace. kalau data yg lain tdk dimasukkan pasti null
        //agar tdk null ya di get dulu data yg dr DB baru di olah dlm proses bisnisnyaa
        MasterVendorInternet masterVendorInternet = vendorInternetRepository.findById(vendorInternetEdit.getId());
        masterVendorInternet.setModifiedOn(currentDate);
        // mapping-kan yang mau di edit
        // user edit di mapping --> user
        modelMapper.map(vendorInternetEdit, masterVendorInternet);
        vendorInternetRepository.save(masterVendorInternet);
        return masterVendorInternet;
    }
    @Override
    public MasterVendorInternet deleteId(Long id) {
        Date currentDate = new Date();
        MasterVendorInternet vendor = vendorInternetRepository.getById(id);
        vendor.setModifiedOn(currentDate);
        vendor.setActive(Boolean.FALSE);
        vendorInternetRepository.save(vendor);
        return vendor;
    }

}
