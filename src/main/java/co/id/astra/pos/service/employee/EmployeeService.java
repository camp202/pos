package co.id.astra.pos.service.employee;

import co.id.astra.pos.model.entity.master.MasterEmployee;
import co.id.astra.pos.model.entity.master.MasterUser;
//import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface EmployeeService  {


    MasterEmployee findById(Long id);

    MasterEmployee save(MasterEmployee employee);
//    User saveId(User user);

    MasterEmployee delete(MasterEmployee employeeDelete);

    MasterEmployee update(MasterEmployee employee);

    List<MasterEmployee> findAll();

//    List<MasterUser> findAllEmployee();

    MasterEmployee edit(MasterEmployee employeeEdit);
}
