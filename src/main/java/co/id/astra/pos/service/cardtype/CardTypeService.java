package co.id.astra.pos.service.cardtype;

import co.id.astra.pos.model.entity.master.MasterCardType;

import java.util.List;

public interface CardTypeService {
    MasterCardType findById(Long id);

    MasterCardType save(MasterCardType masterCardType);

    MasterCardType delete(MasterCardType masterCardType);

    MasterCardType update(MasterCardType masterCardType);

    List<MasterCardType> findAll();

    MasterCardType edit(MasterCardType masterEdit);
}
