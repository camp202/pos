package co.id.astra.pos.service.Item;

import co.id.astra.pos.model.dto.ItemDataListDto;
import co.id.astra.pos.model.dto.ItemDto;
import co.id.astra.pos.model.dto.ItemVarGetInvDto;
import co.id.astra.pos.model.entity.ItemInventory;
import co.id.astra.pos.model.entity.master.MasterItem;
import co.id.astra.pos.model.entity.master.MasterItemVariant;
import co.id.astra.pos.model.entity.master.MasterProvince;
import co.id.astra.pos.service.Item.ItemService;
import co.id.astra.pos.repository.CategoryRepository;
import co.id.astra.pos.repository.ItemInvRepository;
import co.id.astra.pos.repository.ItemRepository;
import co.id.astra.pos.repository.ItemVarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ItemVarRepository itemVarRepository;
    @Autowired
    private ItemInvRepository itemInvRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterItem> findAllItem() { return itemRepository.findAllDataActive(); }

    @Override
    public ItemDto save(ItemDto items) {
        //initiate object
        Date currDate = new Date();
        //createdBy
        //outletId
        MasterItem item =  new MasterItem();

        //save Item
        item.setMasterCategory(categoryRepository.findById(items.getCategoryId()));
        modelMapper.map(items, item);
        //item.setCreatedBy();
        item.setCreatedOn(currDate);
        itemRepository.save(item);

        //get data from list
        List<ItemDataListDto> itemList = items.getItemList();
        for (ItemDataListDto list : itemList) {
            MasterItemVariant itemVar = new MasterItemVariant();
            ItemInventory itemInv = new ItemInventory();

            //save item variant
            itemVar.setMasterItem(itemRepository.findById(item.getId()));
            itemVar.setName(list.getNameItemVar());
            itemVar.setSku(list.getSkuItemVar());
            itemVar.setPrice(list.getPriceItemVar());
            //itemVar.setCreatedBy();
            itemVar.setCreatedOn(currDate);
            itemVarRepository.save(itemVar);

            //save item inventory
            itemInv.setMasterItemVariant(itemVarRepository.findById(itemVar.getId()));
            //itemInv.setMasterOutlet();
            itemInv.setBeginning(list.getBeginningItemInv());
            itemInv.setEndingQty(itemInv.getBeginning());
            itemInv.setAlertAtQty(list.getAlertAtQtyItemInv());
            //itemInv.setCreatedBy();
            itemInv.setCreatedOn(currDate);
            itemInvRepository.save(itemInv);
        }
        return items;
    }

    @Override
    public ItemDto edit(ItemDto items) {
        //initiate object
        Date currDate = new Date();
        //createdBy
        //outletId
        MasterItem item =  itemRepository.findById(items.getId());

        //save Item
        item.setName(items.getName());
        item.setMasterCategory(categoryRepository.findById(items.getCategoryId()));
        //item.setModifiedBy();
        item.setModifiedOn(currDate);
        itemRepository.save(item);

        //get data from list
        List<ItemDataListDto> itemList = items.getItemList();
        for (ItemDataListDto list : itemList) {
            MasterItemVariant itemVar = itemVarRepository.findById(list.getIdItemVar());
            ItemInventory itemInv = itemInvRepository.findById(list.getIdItemInv());

            //save item variant
            itemVar.setPrice(list.getPriceItemVar());
            //itemVar.setModifiedBy();
            itemVar.setModifiedOn(currDate);
            itemVarRepository.save(itemVar);

            //save item inventory
            //itemInv.setMasterOutlet();
            itemInv.setAlertAtQty(list.getAlertAtQtyItemInv());
            //itemInv.setModifiedBy();
            itemInv.setModifiedOn(currDate);
            itemInvRepository.save(itemInv);
        }
        return items;
    }

    @Override
    public ItemDto deleteItem(ItemDto items) {
        Date currDate = new Date();
        MasterItem item =  itemRepository.findById(items.getId());
        List<ItemDataListDto> itemList = items.getItemList();
        for (ItemDataListDto list : itemList) {
            MasterItemVariant itemVar =  itemVarRepository.findById(list.getIdItemVar());
            ItemInventory itemInv =  itemInvRepository.findById(list.getIdItemInv());

            if(itemVar.getActive()==true && itemInv.getEndingQty()!=0)
                return null;
        }
        //item.setModifiedBy();
        item.setModifiedOn(currDate);
        item.setActive(Boolean.FALSE);
        itemRepository.save(item);
        return items;
    }

    @Override
    public ItemDataListDto deleteItem(ItemDataListDto itemData) {
        Date currDate = new Date();

        MasterItemVariant itemVar= itemVarRepository.findById(itemData.getIdItemVar());
        ItemInventory itemInv = itemInvRepository.findById(itemData.getIdItemInv());


        //itemInv.setModifiedBy();
        itemVar.setModifiedOn(currDate);
        itemVar.setActive(Boolean.FALSE);
        itemVarRepository.save(itemVar);

        //itemInv.setModifiedBy();
        itemInv.setModifiedOn(currDate);
        itemInv.setActive(Boolean.FALSE);
        itemInvRepository.save(itemInv);
        return itemData;
    }
}
