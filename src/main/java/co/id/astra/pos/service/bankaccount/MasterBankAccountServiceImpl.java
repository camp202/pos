package co.id.astra.pos.service.bankaccount;


import co.id.astra.pos.model.entity.master.MasterBankAccount;
import co.id.astra.pos.repository.MasterBankAccountRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

@Service
@Transactional
public class MasterBankAccountServiceImpl implements MasterBankAccountService{
    @Autowired
    private MasterBankAccountRepository masterBankAccountRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterBankAccount findById(Long id) {
        return masterBankAccountRepository.findById(id);
    }

    @Override
    public MasterBankAccount save(MasterBankAccount masterBankAccount) {
        Date currentDate = new Date();
        masterBankAccount.setCreatedOn(currentDate);
        masterBankAccount.setActive(Boolean.TRUE);
        return masterBankAccountRepository.save(masterBankAccount);
    }

    @Override
    public MasterBankAccount delete(MasterBankAccount masterBankAccountDelete) {
        Date currentDate = new Date();
        MasterBankAccount masterBankAccount = masterBankAccountRepository.findById(masterBankAccountDelete.getId());
        masterBankAccount.setModifiedOn(currentDate);
        masterBankAccount.setActive(Boolean.FALSE);
        masterBankAccount.setModifiedBy(masterBankAccountDelete.getModifiedBy());
        masterBankAccountRepository.save(masterBankAccount);
        return masterBankAccount;
    }

    @Override
    public MasterBankAccount update(MasterBankAccount masterBankAccount) {
        Date currentDate = new Date();
        return null;
    }

    @Override
    public List<MasterBankAccount> findAll() {

        return masterBankAccountRepository.findAllDataActive();
    }

    @Override
    public List<MasterBankAccount> getById(Long id){
        return masterBankAccountRepository.getById(id);
    }
    @Override
    public MasterBankAccount edit(MasterBankAccount masterBankAccountEdit) {
        Date currentDate = new Date();
        MasterBankAccount masterBankAccount = masterBankAccountRepository.findById(masterBankAccountEdit.getId());
        masterBankAccount.setModifiedOn(currentDate);
        modelMapper.map(masterBankAccountEdit, masterBankAccount);
        masterBankAccountRepository.save(masterBankAccount);
        return masterBankAccount;
    }
}