package co.id.astra.pos.service.district;

import co.id.astra.pos.model.entity.master.MasterDistrict;

import java.util.List;

public interface DistrictService {
    List<MasterDistrict> findAll();
//    List<MasterDistrict> getByIdRegion(Long id);
    List<MasterDistrict> getById(Long id);
    MasterDistrict edit(MasterDistrict masterDistrict);
    MasterDistrict save(MasterDistrict masterDistrict);
}
