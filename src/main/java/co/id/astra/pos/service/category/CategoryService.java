package co.id.astra.pos.service.category;

import co.id.astra.pos.model.entity.master.MasterCategory;

import java.util.List;

public interface CategoryService {
    MasterCategory findById (Long id);
    MasterCategory save(MasterCategory masterCategory);
    MasterCategory delete(MasterCategory masterCategoryDelete);
    MasterCategory deleteId(Long id);
    MasterCategory update(MasterCategory masterCategory);
    List<MasterCategory> findAll();
    List<MasterCategory> getById(Long id);
    MasterCategory edit(MasterCategory masterCategoryEdit);
}
