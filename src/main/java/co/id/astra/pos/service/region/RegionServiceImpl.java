package co.id.astra.pos.service.region;

import co.id.astra.pos.model.entity.master.MasterProvince;
import co.id.astra.pos.model.entity.master.MasterRegion;
import co.id.astra.pos.repository.RegionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RegionServiceImpl implements RegionService {
    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterRegion> findAllByMasterProvince(Long provinceId) {
        MasterProvince masterProvince = new MasterProvince();
        masterProvince.setId(provinceId);
        return regionRepository.findAllByMasterProvince(masterProvince);
    }

    @Override
    public List<MasterRegion> findAllById(Long id) { return regionRepository.findAllById(id); }

    @Override
    public MasterRegion findById(Long id) { return regionRepository.findById(id); }

    @Override
    public MasterRegion findByName(String name) { return regionRepository.findByName(name); }

    @Override
    public MasterRegion save(MasterRegion region) {
        Long id = Long.valueOf(1312500620);
        Date currDate = new Date();
        region.setCreatedBy(id);
        region.setCreatedOn(currDate);
        return regionRepository.save(region);
    }

    @Override
    public List<MasterRegion> findAll() { return regionRepository.findAllDataActive(); }

    @Override
    public MasterRegion delete(MasterRegion regionDelete) {
        Date currDate = new Date();

        MasterRegion region = regionRepository.findById(regionDelete.getId());
        //item.setModifiedBy();
        region.setModifiedOn(currDate);
        region.setActive(Boolean.FALSE);
        regionRepository.save(region);
        return region;
    }

    @Override
    public MasterRegion edit(MasterRegion regionEdit) {
        Date currDate = new Date();

        MasterRegion region = regionRepository.findById(regionEdit.getId());
        //item.setModifiedBy();
        region.setModifiedOn(currDate);
        modelMapper.map(regionEdit, region);
        regionRepository.save(region);
        return region;
    }
}
