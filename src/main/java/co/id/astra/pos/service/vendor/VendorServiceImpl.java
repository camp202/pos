package co.id.astra.pos.service.vendor;

import co.id.astra.pos.model.entity.master.MasterVendor;
import co.id.astra.pos.repository.VendorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class VendorServiceImpl implements VendorService{
    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterVendor findById(Long id) {
        return vendorRepository.findById(id);
    }

    @Override
    public MasterVendor save(MasterVendor masterVendor) {
        Date currentDate = new Date();
        masterVendor.setCreatedOn(currentDate);
        masterVendor.setActive(Boolean.TRUE);
        return vendorRepository.save(masterVendor);
    }

    @Override
    public MasterVendor delete(MasterVendor masterVendorDelete) {
        Date currentDate = new Date();
        MasterVendor masterVendor = vendorRepository.findById(masterVendorDelete.getId());
        masterVendor.setModifiedOn(currentDate);
        masterVendor.setActive(Boolean.FALSE);
        masterVendor.setModifiedBy(masterVendorDelete.getModifiedBy());
        vendorRepository.save(masterVendor);
        return masterVendor;
    }

    @Override
    public MasterVendor update(MasterVendor masterVendor) {
        Date currentDate = new Date();
        return null;
    }

    @Override
    public List<MasterVendor> findAll() {

        return vendorRepository.findAllDataActive();
    }

    @Override
    public List<MasterVendor> getById(Long id){
        return vendorRepository.getById(id);
    }
    @Override
    public MasterVendor edit(MasterVendor vendorEdit) {
        Date currentDate = new Date();
        MasterVendor masterVendor = vendorRepository.findById(vendorEdit.getId());
        masterVendor.setModifiedOn(currentDate);
        modelMapper.map(vendorEdit, masterVendor);
        vendorRepository.save(masterVendor);
        return masterVendor;
    }
}
