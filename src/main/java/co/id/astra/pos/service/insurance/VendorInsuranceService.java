package co.id.astra.pos.service.insurance;

import co.id.astra.pos.model.entity.master.MasterInsurance;

import java.util.List;

public interface VendorInsuranceService {
    List<MasterInsurance> findById (Long id);
    MasterInsurance save (MasterInsurance masterInsurance);
    MasterInsurance delete (MasterInsurance insuranceDelete);
    MasterInsurance update (MasterInsurance masterInsurance);
    List<MasterInsurance> findAll();
    MasterInsurance edit (MasterInsurance insuranceEdit);
//    MasterInsurance getById(Long id);
    MasterInsurance deleteId (Long id);
}
