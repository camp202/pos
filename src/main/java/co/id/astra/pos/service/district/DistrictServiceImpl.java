package co.id.astra.pos.service.district;

import co.id.astra.pos.model.entity.master.MasterDistrict;
import co.id.astra.pos.repository.DistrictRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DistrictServiceImpl implements DistrictService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private DistrictRepository districtRepository;
    @Override
    public List<MasterDistrict> findAll() {
        return districtRepository.findAll();
    }

//    @Override
//    public List<MasterDistrict> getByIdRegion(Long id) {
//        return districtRepository.getByIdRegion(id);
//    }

    @Override
    public List<MasterDistrict> getById(Long id){
        return districtRepository.getById(id);
    }

    @Override
    public MasterDistrict edit(MasterDistrict districtEdit) {
        Date currentDate = new Date();
        MasterDistrict masterDistrict = districtRepository.findById(districtEdit.getId());
        masterDistrict.setModifiedOn(currentDate);
        modelMapper.map(districtEdit, masterDistrict);
        districtRepository.save(masterDistrict);
        return masterDistrict;
    }

    @Override
    public MasterDistrict save(MasterDistrict masterDistrict){
        Date currentDate = new Date();
        masterDistrict.setCreatedOn(currentDate);
        masterDistrict.setActive(Boolean.TRUE);
        return districtRepository.save(masterDistrict);
    }
}
