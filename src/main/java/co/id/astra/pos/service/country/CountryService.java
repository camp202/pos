package co.id.astra.pos.service.country;

import co.id.astra.pos.model.entity.master.MasterCountry;

import java.util.List;

public interface CountryService {
    MasterCountry findById(Long id);
    List<MasterCountry> findAll();
    List<MasterCountry> getById(Long id);
    MasterCountry save(MasterCountry masterCountry);
    MasterCountry findByName (String name);
    MasterCountry delete (MasterCountry countryDelete);
    MasterCountry deleteById (Long id);
    MasterCountry update (MasterCountry masterCountry);
    MasterCountry edit (MasterCountry countryEdit);
}
