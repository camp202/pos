package co.id.astra.pos.service.vendor;

import co.id.astra.pos.model.entity.master.MasterVendor;

import java.util.List;

public interface VendorService {
    MasterVendor findById(Long id);
    MasterVendor save(MasterVendor masterVendor);
    MasterVendor delete(MasterVendor masterVendorDelete);
    MasterVendor update(MasterVendor masterVendor);
    List<MasterVendor> findAll();
    List<MasterVendor>getById(Long id);
    MasterVendor edit(MasterVendor masterVendorEdit);
}
