package co.id.astra.pos.service.employee;

import co.id.astra.pos.model.entity.master.MasterUser;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

//import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    MasterUser findByUsername(String username);

    MasterUser save(MasterUser user);

    List<MasterUser> findAllByIdRole(Long roleId);

    MasterUser delete(MasterUser userDelete);

    MasterUser update(MasterUser user);

    List<MasterUser> findAll();

    MasterUser edit(MasterUser userEdit);

    MasterUser editLock(MasterUser unlockUser);
}
