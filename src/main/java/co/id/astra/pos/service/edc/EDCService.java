package co.id.astra.pos.service.edc;

import co.id.astra.pos.model.entity.master.MasterEDC;

import java.util.List;

public interface EDCService {
    List<MasterEDC> findAll();
    MasterEDC save(MasterEDC edc);
    MasterEDC findById(Long id);
    MasterEDC edit(MasterEDC masterEDCEdit);
}
