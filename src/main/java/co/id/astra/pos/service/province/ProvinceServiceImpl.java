package co.id.astra.pos.service.province;

import co.id.astra.pos.model.entity.master.MasterProvince;
import co.id.astra.pos.repository.ProvinceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ProvinceServiceImpl implements ProvinceService {
    @Autowired
    private ProvinceRepository provinceRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterProvince> findById(Long id) { return provinceRepository.findAllById(id); }

    @Override
    public MasterProvince findByName(String name) { return provinceRepository.findByName(name); }

    @Override
    public MasterProvince save(MasterProvince province) {
        Date currDate = new Date();
        //item.setCreatedBy();
        province.setCreatedOn(currDate);
        return provinceRepository.save(province);
    }

    @Override
    public List<MasterProvince> findAll() { return provinceRepository.findAllDataActive(); }

    @Override
    public MasterProvince delete(MasterProvince provinceDelete) {

        Date currDate = new Date();

        MasterProvince province = provinceRepository.findById(provinceDelete.getId());
        //item.setModifiedBy();
        province.setModifiedOn(currDate);
        province.setActive(Boolean.FALSE);
        provinceRepository.save(province);
        return province;
    }

    @Override
    public MasterProvince edit(MasterProvince provinceEdit) {
        Date currDate = new Date();

        MasterProvince province = provinceRepository.findById(provinceEdit.getId());
        //item.setModifiedBy();
        province.setModifiedOn(currDate);
        modelMapper.map(provinceEdit, province);
        provinceRepository.save(province);
        return province;
    }
}
