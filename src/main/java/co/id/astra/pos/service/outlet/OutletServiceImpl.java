package co.id.astra.pos.service.outlet;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import co.id.astra.pos.repository.OutletRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class OutletServiceImpl implements OutletService {
    @Autowired
    private OutletRepository outletRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterOutlet findById(Long id) {
        return outletRepository.findById(id);
    }

    @Override
    public MasterOutlet save(MasterOutlet masterOutlet) {
        Date currentDate = new Date();
        masterOutlet.setCreatedOn(currentDate);
        masterOutlet.setActive(Boolean.TRUE);
        return outletRepository.save(masterOutlet);
    }

    @Override
    public MasterOutlet delete(MasterOutlet outletDelete) {
        Date currentDate = new Date();
        // informasi deletenya jangan lupa diberikan
        // delete itu actualnya cuma ganti status
        MasterOutlet masterOutlet = outletRepository.findById(outletDelete.getId());
        masterOutlet.setModifiedOn(currentDate);
        masterOutlet.setActive(Boolean.FALSE);
        masterOutlet.setModifiedBy(outletDelete.getModifiedBy());
        outletRepository.save(masterOutlet);

        return masterOutlet;
    }

    @Override
    public MasterOutlet update(MasterOutlet masterOutlet) {
        //Jika ada updete keterangan modifiednya jangan lupa di update
        Date currentDate = new Date();

        return null;
    }

    @Override
    public List<MasterOutlet> findAll() {
        return outletRepository.findAllDataActive();
    }


    public MasterOutlet edit(MasterOutlet outletEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya agar saat di edit data yang lama tidak null
        // karna yg diedit kan cuma sebagian data, nah kalau data yg lama pasti ter-replace. kalau data yg lain tdk dimasukkan pasti null
        //agar tdk null ya di get dulu data yg dr DB baru di olah dlm proses bisnisnyaa
        MasterOutlet masterOutlet = outletRepository.findById(outletEdit.getId());
        masterOutlet.setModifiedOn(currentDate);
        // mappingkan yang mau di edit
        // useredit di mapping --> user
        modelMapper.map(outletEdit, masterOutlet);
        outletRepository.save(masterOutlet);
        return masterOutlet;
    }
}
