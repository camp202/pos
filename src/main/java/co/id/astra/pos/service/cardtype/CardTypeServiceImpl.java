package co.id.astra.pos.service.cardtype;

import co.id.astra.pos.model.entity.master.MasterCardType;
import co.id.astra.pos.model.entity.master.MasterOutlet;
import co.id.astra.pos.repository.CardTypeRepository;
import co.id.astra.pos.repository.OutletRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CardTypeServiceImpl implements CardTypeService{
    @Autowired
    private CardTypeRepository cardTypeRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterCardType findById(Long id) {
        return cardTypeRepository.findById(id);
    }

    @Override
    public MasterCardType save(MasterCardType masterCardType) {
        Date currentDate = new Date();
        masterCardType.setCreatedOn(currentDate);
        masterCardType.setActive(Boolean.TRUE);
        return cardTypeRepository.save(masterCardType);
    }

    @Override
    public MasterCardType delete(MasterCardType cardTypeDelete) {
        Date currentDate = new Date();
        // informasi deletenya jangan lupa diberikan
        // delete itu actualnya cuma ganti status
        MasterCardType masterCardType = cardTypeRepository.findById(cardTypeDelete.getId());
        masterCardType.setModifiedOn(currentDate);
        masterCardType.setActive(Boolean.FALSE);
//        masterCardType.setModifiedBy(cardTypeDelete.getModifiedBy());
        cardTypeRepository.save(masterCardType);

        return masterCardType;
    }

    @Override
    public MasterCardType update(MasterCardType masterCardType) {
        //Jika ada updete keterangan modifiednya jangan lupa di update
        Date currentDate = new Date();

        return null;
    }

    @Override
    public List<MasterCardType> findAll() {
        return cardTypeRepository.findAllDataActive();
    }


    public MasterCardType edit(MasterCardType cardTypeEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya agar saat di edit data yang lama tidak null
        // karna yg diedit kan cuma sebagian data, nah kalau data yg lama pasti ter-replace. kalau data yg lain tdk dimasukkan pasti null
        //agar tdk null ya di get dulu data yg dr DB baru di olah dlm proses bisnisnyaa
        MasterCardType masterCardType = cardTypeRepository.findById(cardTypeEdit.getId());
        masterCardType.setModifiedOn(currentDate);
        // mappingkan yang mau di edit
        // useredit di mapping --> user
        modelMapper.map(cardTypeEdit, masterCardType);
        cardTypeRepository.save(masterCardType);
        return masterCardType;
    }
}
