package co.id.astra.pos.service.Barcode;

import co.id.astra.pos.model.entity.master.MasterBarcode;
import co.id.astra.pos.repository.BarcodeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BarcodeServiceImpl implements BarcodeService{
    @Autowired
    private BarcodeRepository barcodeRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MasterBarcode> findById(Long id) {
        return barcodeRepository.findById(id);
    }

    @Override
    public MasterBarcode getById(Long id){
        return barcodeRepository.getById(id);
    }

    @Override
    public MasterBarcode save(MasterBarcode barcode) {
        Date currentDate = new Date();
        barcode.setActive(Boolean.TRUE);
        barcode.setCreatedOn(currentDate);
        return barcodeRepository.save(barcode);
    }

    @Override
    public List<MasterBarcode> findAll() {
        return barcodeRepository.findAllDataActive();
    }


    @Override
    public MasterBarcode edit(MasterBarcode barcodeEdit) {
        Date currentDate = new Date();
        MasterBarcode barcode = barcodeRepository.getById(barcodeEdit.getId());
        barcode.setModifiedOn(currentDate);
        modelMapper.map(barcodeEdit, barcode);
        barcodeRepository.save(barcode);
        return barcode;
    }

    @Override
    public MasterBarcode update(MasterBarcode barcode){
        Date currentDate = new Date();
        return null;
    }

    @Override
    public MasterBarcode deleteId(Long id) {
        Date currentDate = new Date();
        MasterBarcode barcode = barcodeRepository.getById(id);
        barcode.setModifiedOn(currentDate);
        barcode.setActive(Boolean.FALSE);
        barcodeRepository.save(barcode);
        return barcode;
    }
}


