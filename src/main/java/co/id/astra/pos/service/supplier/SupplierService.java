package co.id.astra.pos.service.supplier;


import co.id.astra.pos.model.entity.master.MasterSupplier;

import java.util.Arrays;
import java.util.List;


public interface SupplierService {
    MasterSupplier save(MasterSupplier supplier);
    MasterSupplier update(MasterSupplier supplier);
    List<MasterSupplier> findAll();
    MasterSupplier edit(MasterSupplier supplierEdit);
    MasterSupplier deleteId(Long Id);
    List<MasterSupplier> findById(Long id);
    MasterSupplier getById(Long Id);

}
