package co.id.astra.pos.service.Barcode;

import co.id.astra.pos.model.entity.master.MasterBarcode;

import java.util.List;

public interface BarcodeService {
    MasterBarcode save(MasterBarcode barcode);
    MasterBarcode update(MasterBarcode barcode);
    List<MasterBarcode> findAll();
    MasterBarcode edit(MasterBarcode barcodeEdit);
    MasterBarcode deleteId(Long Id);
    List<MasterBarcode> findById(Long id);
    MasterBarcode getById(Long Id);
}
