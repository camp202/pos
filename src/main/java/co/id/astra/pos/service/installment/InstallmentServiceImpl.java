package co.id.astra.pos.service.installment;

import co.id.astra.pos.model.entity.master.MasterInstallment;
import co.id.astra.pos.repository.InstallmentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class InstallmentServiceImpl implements InstallmentService {
    @Autowired
    InstallmentRepository installmentRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasterInstallment findById(Long id){return installmentRepository.findById(id);}

    @Override
    public MasterInstallment save(MasterInstallment installment){
        Date currentDate = new Date();
        installment.setCreatedOn(currentDate);
        return installmentRepository.save(installment);
    }

//    @Override
//    public MasterInstallment delete(MasterInstallment installmentDelete){
//        Date currentDate = new Date();
//        MasterInstallment installment = installmentRepository.findById(installmentDelete.getId());
//        installment.setActive(Boolean.FALSE);
//        installment.setModifiedOn(currentDate);
//        installment.setModifiedBy(installmentDelete.getModifiedBy());
//        installmentRepository.save(installment);
//
//        return installment;
//
//    }

    @Override
    public MasterInstallment update(MasterInstallment installment) {return null;}

    @Override
    public List<MasterInstallment>findAll(){return installmentRepository.findAll();}

    @Override
    public List<MasterInstallment>getById(Long id) {return installmentRepository.getById(id);}

    @Override
    public MasterInstallment edit(MasterInstallment installmentEdit) {
        MasterInstallment installment = installmentRepository.findById(installmentEdit.getId());
        modelMapper.map(installmentEdit, installment);
        installmentRepository.save(installment);
        return installment;
    }
}
